# Zaccheo CRM
Zaccheo è un software pensato per la gestione tramite tessere a punti di un emporio del vestiario solidale Caritas.
Zaccheo è costruito utilizzando come motore grafico per l'interfaccia la libreria [PySimpleGUI](https://github.com/PySimpleGUI/PySimpleGUI).

Zaccheo permette di:
- registrare gli utenti con la loro anagrafica;
- creare le tessere associate ad ogni utente;
- creare un elenco di *prodotti* ciascuno con un punteggio associato;
- effettuare gli *scontrini* dei prodotti acquistati dagli utenti con aggiornamento automatico del punteggio sulla tessera.

## Installazione
È possibile scaricare Zaccheo tramite la [pagina delle release](https://gitlab.com/caritas-erba/zaccheo-crm/-/releases). Una volta sulla pagina, scaricare l'installer associato alla versione più recente.

Una volta scaricato l'installer sul pc, eseguirlo e seguire la procedura guidata. Tenere traccia del percorso della cartella di installazione, perché servirà in seguito.

### Variabili d'ambiente
Zaccheo necessita di configurare alcune variabili d'ambiente per funzionare correttamente.

Per poter configurare le variabili d'ambiente, eseguire i seguenti passi contenuti in questa [guida](https://nonsoloprogrammi.net/variabili-dambiente-windows) o in altre analoghe che si possono trovare sul web.

Le nuove variabili d'ambiente da creare sono:

1. `ZACCHEO_CARITAS_GROUP`: nome del gruppo caritas.
2. `ZACCHEO_DATA_DIR`: percorso ad una cartella dove saranno salvati i dati generati da Zaccheo.
3. `ZACCHEO_WORKING_DIR`: percorso assoluto alla cartella di lavoro del codice. La variabile va configurata con il valore `<percorso-di-installazione>\pkgs`.
4. `ZACCHEO_CARD_ADULT_POINTS`: numero mensile di punti da associare ad ogni adulto.
5. `ZACCHEO_CARD_CHILD_POINTS`: numero mensile di punti da associare ad ogni bambino.
6. `ZACCHEO_CARD_CHILD_AGE`: età fino alla quale una persona è considerata bambino.
7. `ZACCHEO_CARD_MONTHS_VALIDITY`: validità della tessera (in mesi).
8. `ZACCHEO_CARD_CUSTOM_TITLE`: intestazione (titolo) da mostrare sulla tessera stampata; utilizzare una stringa di 30 caratteri (spazi compresi) al **massimo**.
8. `ZACCHEO_DRIVE_IS_ACTIVE`: configurare questa variabile a `false` se non si intende utilizzare la funzionalità di caricamento ultimi accessi su Google Drive, altrimenti configurarla a `true`. Se la variabile è a `false`, non è necessario creare le variabili seguenti.
9. `ZACCHEO_DRIVE_TEAM_ID`: codice identificativo del team (cartella condivisa) su Google Drive.
10. `ZACCHEO_DRIVE_PARENT_FOLDER_ID`: codice identificativo della cartella di Google Drive nella quale si vogliono salvare i file degli ultimi accessi.
11. `ZACCHEO_DRIVE_CLIENT_ID`: codice ID del client API di Google che si intende utilizzare per l'accesso a Google Drive.
12. `ZACCHEO_DRIVE_CLIENT_SECRET`: codice segreto associato al client API di Google utilizzato.

## Manuale utente
Zaccheo è provvisto di un manuale utente che illustra il funzionamento del programma e le sue funzionalità. È possibile scaricarlo dalla stessa [pagina](https://gitlab.com/caritas-erba/zaccheo-crm/-/releases) in cui è presente l'installer. Una volta scaricato lo zip, estrarlo in una cartella a propria scelta. Per visualizzare il manuale, entrare nella cartella estratta e aprire il file (con un doppio click) *index.html*.
## Sviluppo

### Linux

#### Versione Python
Python 3.6 è raccomandato, poichè le tabelle di `PySimpleGUI` sulla porta tkinter funzionano al meglio con questa versione.

#### Dependenze
Per eseguire il codice associato alla libreria `PySimpleGUI` è necessario installare alcuni pacchetti esterni:
```
sudo apt-get install python3-tk
```
Inoltre, se si vuole costruire l'installer `.exe` su Linux, è necessario anche:
```
sudo apt-get install nsis
```

#### Variabili d'ambiente
Per eseguire correttamente il software, devono essere configurate le variabili d'ambiente indicate precedentemente.

#### Packaging
Tutte le dipendenze Python devono essere elencate, con la versione utilizzata, nell'`installer.cfg`. Nota che, per alcuni pacchetti, devono essere incluse anche le loro principali sottodipendenze, altrimenti l'eseguibile, una volta installato, non funzionerà.

La cartella `wheels` nella cartella principale di progetto contiene le wheels di alcuni pacchetti che sono scaricati come `tar.gz` da PyPi. Le wheels corrispondenti, buildate localmente, devono essere incluse nella cartella `wheels` e committate per permettere al progetto di buildare correttamente. Il file `installer.cfg` contiene una chiave `extra_wheel_sources` che elenca le cartelle dove ricercare ulteriori wheels, se non riesce a trovarle su PyPi.
#### Versioning
Aggiornare la chiave `version` nel file `installer.cfg`.

#### Imports
Per funzionare correttamente su Windows, tutti gli import devono cominciare con `src.`.

## Contribuisci
Zaccheo è un software open source.
Puoi contribuire a questo progetto nei modi usuali, ad esempio aprendo nuovi issue per risoluzione bug o richiesta nuove features oppure tramite merge requests.

Ogni contributo e nuova idea sono molto apprezzati!
