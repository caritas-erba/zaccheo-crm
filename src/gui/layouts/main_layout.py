import PySimpleGUI as sg

from src.gui.layouts.constants import FONT
from src.gui.keys.constants import EXIT_EVENT
from src.gui.layouts.products_layout import ProductsLayout
from src.gui.layouts.anagraphic_layout import AnagraphicLayout
from src.gui.layouts.cards_layout import CardsLayout
from src.gui.layouts.tickets_layout import TicketsLayout
from src.gui.keys.main_tab_keys import MainTabKeys as mtk


class MainLayout:

    layout = [
        [
            sg.Column(layout=[
                [
                    sg.TabGroup([[
                        sg.Tab("Anagrafica", AnagraphicLayout.layout, key=mtk.anagraphic_tab, font=FONT),
                        sg.Tab("      Tessere", CardsLayout.layout, key=mtk.card_keys, font=FONT),
                        sg.Tab("     Prodotti", ProductsLayout.layout, key=mtk.product_keys, font=FONT),
                        sg.Tab("    Scontrini", TicketsLayout.layout, key=mtk.ticker_keys, font=FONT),
                    ]],
                        tab_location="lefttop", font=FONT)
                ],

                [
                    sg.Column(layout=[[sg.Button('Esci', key=EXIT_EVENT, font=FONT)]],
                              justification="center")
                ]
            ],
                size=(850, 650),
                scrollable=True)
        ]
    ]
