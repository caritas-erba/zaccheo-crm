import PySimpleGUI as sg

from src.gui.layouts.constants import (HEADER_TEXT_SIZE, HEADER_FONT, HEADER_JUSTIFICATION, FONT, TEXT_SIZE,
                                       FRAME_TITLE_FONT, DOUBLE_COLUMN_WIDTH, TABLE_HEADER_FONT,
                                       ALL_WINDOW_SIZE, FRAME_TITLE_COLOR)
from src.gui.components.find_from_input import find_from_input_with_text
from src.gui.keys.ticket_keys import TicketsLayoutKeys as tlk
from src.data.headers.products_header import ProductsHeader as ph
from src.data.headers.ticket_header import SummaryTicketTableHeader as stth


class TicketsLayout:

    tab_view_ticket = [
        [sg.Frame(title="Ricerca", font=FRAME_TITLE_FONT, title_color=FRAME_TITLE_COLOR, layout=[

            find_from_input_with_text("Id Tessera", input_key=tlk.input_view_card_id,
                                      button_find_key=tlk.button_view_find_card)
        ])],

        [sg.Column(layout=[
            [sg.Text("Data attivazione servizio", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_view_service_start_date, size=(10, 1), font=FONT,
                      justification="right", disabled=True)],

            [sg.Text("Data fine servizio", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_view_service_end_date, size=(10, 1), font=FONT,
                      justification="right", disabled=True)],

            [sg.Text("Totale punti utilizzati", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_view_total_points_spent, size=(10, 1), font=FONT,
                      justification="right", disabled=True)],

            [sg.Text("Punti disponibili", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_view_available_points, size=(10, 1), font=FONT,
                      justification="right", disabled=True)],

            [sg.Text()],

            [sg.Text("Punti scontrino selezionato", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_view_ticket_total_points, size=(10, 1), font=FONT,
                      justification="right", disabled=True)],
        ], vertical_alignment="top"),
         sg.Column(layout=[
             [sg.Text("Selezionare una data dall'elenco per visualizzare i dettagli dello scontrino.",
                      size=(28, 2), font=FONT)],

             [sg.Listbox([], key=tlk.output_past_ticket_dates_list, size=(27, 6), font=FONT,
                         enable_events=True)]
         ])],

        [sg.Table(key=tlk.output_past_ticket_summary_table,
                  values=[],
                  headings=stth.all,
                  num_rows=15,
                  col_widths=stth.column_sizes,
                  justification="left",
                  font=FONT,
                  header_font=TABLE_HEADER_FONT,
                  auto_size_columns=False,
                  enable_events=False)]
    ]

    tab_insert_ticket = [
        [sg.Frame(title="Ricerca", font=FRAME_TITLE_FONT, title_color=FRAME_TITLE_COLOR, layout=[

            find_from_input_with_text("Id Tessera", input_key=tlk.input_insert_card_id, button_find_key=tlk.button_insert_find_card),

            [sg.Text("Data ultimo accesso", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_last_access_date, size=(10, 1), font=FONT, disabled=True),
             sg.Text("Punti disponibili", size=TEXT_SIZE, font=FONT, justification="right"),
             sg.Input(key=tlk.output_insert_available_points, size=(8, 1), font=FONT, disabled=True)],

            [sg.Text("Data fine servizio", size=TEXT_SIZE, font=FONT),
             sg.Input(key=tlk.output_insert_service_end_date, size=(10, 1), font=FONT, disabled=True),
             sg.Button("Nuovo scontrino", key=tlk.button_new_ticket, size=(27, 1), font=FONT, disabled=True)]
        ])],

        [sg.Text("Selezionare 'Destinatario' e 'Categoria' per visualizzare i prodotti disponibili. Selezionare "
                 "poi un prodotto e inserire la quantità per aggiungerlo allo scontrino.",
                 size=(60, 2), font=FONT)],

        [sg.Column(layout=[
            [sg.Text("Destinatario", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

            [sg.Listbox(ph.target_set, key=tlk.input_target, size=(DOUBLE_COLUMN_WIDTH, 4), font=FONT, disabled=True,
                        enable_events=True)],

            [sg.Text("Categoria", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

            [sg.Listbox(ph.categories_set, key=tlk.input_category, size=(DOUBLE_COLUMN_WIDTH, 4),
                        font=FONT, disabled=True, enable_events=True)],
        ]),

         sg.Column(layout=[
             [sg.Text("Prodotto", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

             [sg.Listbox([], key=tlk.input_product, size=(DOUBLE_COLUMN_WIDTH, 7), font=FONT, disabled=True)],

             [sg.Text("Quantità", size=TEXT_SIZE, font=FONT),
              sg.Input(key=tlk.input_quantity, size=(9, 1), font=FONT, disabled=True, enable_events=True)],

             [sg.Button("Aggiungi articolo", key=tlk.button_add_article, size=(28, 1), font=FONT, disabled=True)]

         ])],

        [sg.Frame(title="Riepilogo", font=FRAME_TITLE_FONT, title_color=FRAME_TITLE_COLOR, layout=[
            [sg.Table(key=tlk.output_ticket_summary_table,
                      values=[],
                      headings=stth.all,
                      num_rows=10,
                      col_widths=stth.column_sizes,
                      justification="left",
                      font=FONT,
                      header_font=TABLE_HEADER_FONT,
                      auto_size_columns=False,
                      enable_events=False)],

            [sg.Button("Rimuovi articolo", key=tlk.button_remove_article, size=ALL_WINDOW_SIZE, font=FONT, disabled=True)],

            [sg.Text("Punti totali scontrino", size=(17, 1), font=FONT),
             sg.Input(default_text="0", key=tlk.output_insert_ticket_total_points, size=(6, 1), font=FONT,
                      justification="right", disabled=True),
             sg.Button("Salva scontrino e stampa tessera aggiornata", key=tlk.button_save_and_print_ticket,
                       size=(33, 1), font=FONT, disabled=True)]
        ])]

    ]

    layout = [
        [sg.Text("Scontrini", size=HEADER_TEXT_SIZE, font=HEADER_FONT, justification=HEADER_JUSTIFICATION)],

        [
            sg.TabGroup([
                [sg.Tab("Nuovo scontrino", tab_insert_ticket, key=222, font=FONT),
                 sg.Tab("Ricerca scontrino", tab_view_ticket, key=1111, font=FONT)]
            ],
                tab_location="topleft", font=FONT)
        ]
    ]