import PySimpleGUI as sg

from src.gui.layouts.constants import (HEADER_TEXT_SIZE, HEADER_FONT, HEADER_JUSTIFICATION, TEXT_SIZE, FONT,
                                       INPUT_SIZE, ALL_WINDOW_SIZE, ATTENTION_TEXT_COLOR, LISTBOX_SIZE,
                                       CALENDAR_DAY_NAMES, CALENDAR_MONTH_NAMES, CALENDAR_DATE_FORMAT,
                                       DOUBLE_LINE_TEXT_SIZE, TABLE_HEADER_FONT, CALENDAR_DATE_DEFAULT,
                                       FRAME_TITLE_FONT, FRAME_TITLE_COLOR, HALF_WINDOW_SIZE,
                                       DELETE_BUTTON_COLOR)
from src.gui.keys.anagraphic_keys import AnagraphicLayoutKeys as alk
from src.data.headers.anagraphic_header import AnagraphicHeader as ah, FamilyTableHeader as fth
from src.gui.components.calendar_components import calendar_input


class AnagraphicLayout:

    tab_insert_family_layout = [

        [sg.Text("Inserire un componente della famiglia alla volta. Clicca su 'Fine' una volta inseriti "
                 "tutti i componenti (oppure per salvare un famigliare aggiunto in seguito). \n"
                 "Clicca su 'Aggiorna' per aggiornare i dati di un componente già esistente.",
                 size=(60, 3), font=FONT)],

        [sg.Button("Nuova famiglia", key=alk.button_new_family, size=ALL_WINDOW_SIZE, font=FONT)],

        [sg.Text("Nome", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_name_insert, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Cognome", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_surname_insert, size=INPUT_SIZE, font=FONT, disabled=True)],

        [
            sg.Text("Sesso", size=(7, 1), font=FONT),
            sg.Radio("M", group_id="sex", key=alk.radio_sex_male, font=FONT, disabled=True),
            sg.Radio("F", group_id="sex", key=alk.radio_sex_female, font=FONT, disabled=True),

            sg.Text("", size=(4, 1)),  # Blank space
            sg.Text("Data di Nascita", size=(13, 1), font=FONT),
            sg.Input(default_text=CALENDAR_DATE_DEFAULT, key=alk.input_birth_date_insert, size=(11, 10),
                     font=FONT, disabled=True),
            sg.CalendarButton("Calendario", size=(9, 1), font=FONT, format=CALENDAR_DATE_FORMAT,
                              begin_at_sunday_plus=1, month_names=CALENDAR_MONTH_NAMES,
                              day_abbreviations=CALENDAR_DAY_NAMES)
        ],

        [sg.Text("Ruolo famigliare", size=TEXT_SIZE, font=FONT),
         sg.Listbox(ah.family_role_set, key=alk.input_family_role, default_values=["Capofamiglia"],
                    size=LISTBOX_SIZE, font=FONT, disabled=True)],

        [sg.Text("Residenza", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_residence, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Cittadinanza", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_citizenship, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Nazione di origine", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_origin_country, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Numero di cellulare", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_mobile, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("E-mail", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_email, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Tipo documento di identità", size=DOUBLE_LINE_TEXT_SIZE, font=FONT),
         sg.Listbox(ah.id_document_type_set, key=alk.input_id_doc_type, default_values=["Nessuno"],
                    size=LISTBOX_SIZE, font=FONT, disabled=True)],

        [sg.Text("Numero documento di identità", size=DOUBLE_LINE_TEXT_SIZE, font=FONT),
         sg.Input(key=alk.input_id_doc_number, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Button("Salva famigliare e inserisci nuovo", key=alk.button_new_component,
                   size=ALL_WINDOW_SIZE, font=FONT, disabled=True)],

        [sg.Button("Fine", key=alk.button_save_family, size=HALF_WINDOW_SIZE, font=FONT, disabled=True),
         sg.Button("Aggiorna", key=alk.button_save_updated_component, size=HALF_WINDOW_SIZE, font=FONT, disabled=True)]
    ]

    tab_view_family_layout = [

        [sg.Frame(title="Ricerca", font=FRAME_TITLE_FONT, title_color=FRAME_TITLE_COLOR, layout=[

            [sg.Text("Inserire Nome, Cognome e Data di Nascita per effettuare una ricerca:",
                     size=ALL_WINDOW_SIZE, font=FONT)],

            [sg.Text("Nome", size=TEXT_SIZE, font=FONT),
             sg.Input(key=alk.input_name_view, size=INPUT_SIZE, font=FONT)],

            [sg.Text("Cognome", size=TEXT_SIZE, font=FONT),
             sg.Input(key=alk.input_surname_view, size=INPUT_SIZE, font=FONT)],

            calendar_input(text="Data di Nascita", input_key=alk.input_birth_date_view),

            [sg.Button("Cerca", key=alk.button_search_family, size=ALL_WINDOW_SIZE, font=FONT, bind_return_key=True)]
        ])],

        [sg.Text("", key=alk.text_search_response, size=(60, 2), font=FONT,
                 text_color=ATTENTION_TEXT_COLOR)],

        [sg.Table(key=alk.table_family,
                  values=[],
                  headings=fth.all,
                  num_rows=10,
                  col_widths=fth.column_sizes,
                  justification="left",
                  font=FONT,
                  header_font=TABLE_HEADER_FONT,
                  auto_size_columns=False,
                  enable_events=False)],

        [sg.Text("Telefono capofamiglia", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.output_mobile, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Email capofamiglia", size=TEXT_SIZE, font=FONT),
         sg.Input(key=alk.output_email, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Button("Modifica familiare", key=alk.button_modify_component, size=HALF_WINDOW_SIZE,
                   font=FONT, disabled=True),
         sg.Button("Aggiungi familiare", key=alk.button_add_component, size=HALF_WINDOW_SIZE, font=FONT,
                   disabled=True)],

        [sg.Button("Associa Tessera al capofamiglia", key=alk.button_bind_card, size=HALF_WINDOW_SIZE, font=FONT,
                   disabled=True),
         sg.Button("Elimina familiare", key=alk.button_delete_component, size=HALF_WINDOW_SIZE, font=FONT,
                   disabled=True, button_color=DELETE_BUTTON_COLOR)
        ]
    ]

    layout = [

        [sg.Text("Anagrafica", size=HEADER_TEXT_SIZE, font=HEADER_FONT, justification=HEADER_JUSTIFICATION)],

        [
            sg.TabGroup([
                [sg.Tab("Ricerca famiglia", tab_view_family_layout, key=alk.tab_view_family, font=FONT),
                 sg.Tab("Inserisci famiglia", tab_insert_family_layout, key=alk.tab_insert_family, font=FONT,)]
            ],
                tab_location="topleft", font=FONT)
        ]
    ]
