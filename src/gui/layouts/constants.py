# SIZES
TEXT_SIZE = (20, 1)
DOUBLE_LINE_TEXT_SIZE = (20, 2)
ALL_WINDOW_SIZE = (61, 1)
HALF_WINDOW_SIZE = (30, 1)
THIRD_WINDOW_SIZE = (19, 1)
HEADER_TEXT_SIZE = (45, 1)
INPUT_SIZE = (40, 1)
LISTBOX_SIZE = (40, 5)
DOUBLE_COLUMN_WIDTH = 29

# FONT
FONT = ("Montserrat", 11)
FRAME_TITLE_FONT = ("Montserrat Bold", 12)
HEADER_FONT = ("Montserrat Bold", 16)
HEADER_JUSTIFICATION = "center"
TABLE_HEADER_FONT = ("Montserrat Bold", 13)

# COLORS
ATTENTION_TEXT_COLOR = "yellow"
FRAME_TITLE_COLOR = "blue"
DELETE_BUTTON_COLOR = ("white", "#bf3424")

# POPUP
POPUP_WARNING_TITLE = "Attenzione"
POPUP_SUCCESS_TITLE = "Ok"
POPUP_ERROR_TITLE = "Errore"

# CALENDAR
CALENDAR_MONTH_NAMES = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                        "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
CALENDAR_DAY_NAMES = ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"]
CALENDAR_DATE_FORMAT = "%d/%m/%Y"
CALENDAR_DATE_DEFAULT = "gg/mm/aaaa"
