import PySimpleGUI as sg

from src.gui.layouts.constants import (HEADER_FONT, HEADER_JUSTIFICATION, FONT, TEXT_SIZE, ALL_WINDOW_SIZE,
                                       HEADER_TEXT_SIZE, INPUT_SIZE, THIRD_WINDOW_SIZE,
                                       FRAME_TITLE_FONT, DOUBLE_COLUMN_WIDTH, FRAME_TITLE_COLOR,
                                       DELETE_BUTTON_COLOR)
from src.gui.keys.products_keys import ProductsLayoutKeys as plk
from src.data.headers.products_header import ProductsHeader as ph


class ProductsLayout:
    """ Defines the layout for the product tab """

    layout = [

        [sg.Text("Prodotti", size=HEADER_TEXT_SIZE,
                 font=HEADER_FONT, justification=HEADER_JUSTIFICATION)],

        [sg.Frame(title="Ricerca", font=FRAME_TITLE_FONT, title_color=FRAME_TITLE_COLOR, layout=[

            [sg.Text("Selezionare Destinatario e Categoria per visualizzare i prodotti già presenti a catalogo. \n"
                     "Selezionare un prodotto per visualizzarne i dettagli ed eventualmente modificarlo o eliminarlo. "
                     "E' sempre prossibile creare un nuovo prodotto.",
                     size=(60, 4), font=FONT)],

            [sg.Column(layout=[
                [sg.Text("Destinatario", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

                [sg.Listbox(ph.target_set, key=plk.select_target, size=(DOUBLE_COLUMN_WIDTH, 4), font=FONT,
                            enable_events=True)],

                [sg.Text("Categoria", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

                [sg.Listbox(ph.categories_set, key=plk.select_category, size=(DOUBLE_COLUMN_WIDTH, 4),
                            font=FONT, enable_events=True)],
            ]),

            sg.Column(layout=[
                [sg.Text("Prodotto", size=(DOUBLE_COLUMN_WIDTH, 1), font=FONT, justification="center")],

                [sg.Listbox([], key=plk.select_product, size=(DOUBLE_COLUMN_WIDTH, 10), font=FONT,
                            enable_events=True)]
            ])],

            [sg.Button("Nuovo prodotto", key=plk.button_create_new, size=ALL_WINDOW_SIZE, font=FONT)]
            ])
        ],

        [sg.Text("")],

        [sg.Text("Nome prodotto", size=TEXT_SIZE, font=FONT),
         sg.Input(key=plk.input_product, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Descrizione", size=TEXT_SIZE, font=FONT),
         sg.Input(key=plk.input_description, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Punti", size=TEXT_SIZE, font=FONT),
         sg.Input(key=plk.input_points, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Text("Max acquistabili", size=TEXT_SIZE, font=FONT),
         sg.Input(key=plk.input_max_purchasable, size=INPUT_SIZE, font=FONT, disabled=True)],

        [sg.Button('Salva', key=plk.button_save, size=THIRD_WINDOW_SIZE, font=FONT, disabled=True),
         sg.Button("Aggiorna", key=plk.button_update, size=THIRD_WINDOW_SIZE, font=FONT, disabled=True),
         sg.Button("Elimina", key=plk.button_delete, size=THIRD_WINDOW_SIZE, font=FONT, disabled=True,
                   button_color=DELETE_BUTTON_COLOR)]
    ]

    def __init__(self):
        pass
