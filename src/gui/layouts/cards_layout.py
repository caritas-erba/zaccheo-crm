import PySimpleGUI as sg

from src.gui.layouts.constants import (
    HEADER_FONT,
    HEADER_JUSTIFICATION,
    HEADER_TEXT_SIZE,
    TEXT_SIZE,
    FONT,
    INPUT_SIZE,
    ALL_WINDOW_SIZE,
    ATTENTION_TEXT_COLOR,
    FRAME_TITLE_FONT,
    FRAME_TITLE_COLOR,
    DOUBLE_COLUMN_WIDTH,
)
from src.data.headers.cards_header import CardsHeader as ch
from src.gui.keys.cards_keys import CardsLayoutKeys as clk
from src.gui.components.calendar_components import calendar_input
from src.utils.list import rearrange_list_in_columns


class CardsLayout:
    layout = [
        [
            sg.Text(
                "Tessere",
                size=HEADER_TEXT_SIZE,
                font=HEADER_FONT,
                justification=HEADER_JUSTIFICATION,
            )
        ],
        [
            sg.Frame(
                title="Ricerca",
                font=FRAME_TITLE_FONT,
                title_color=FRAME_TITLE_COLOR,
                layout=[
                    [
                        sg.Text(
                            "Effettua ricerca tramite Numero Tessera:",
                            size=ALL_WINDOW_SIZE,
                            font=FONT,
                        )
                    ],
                    # Using columns also for one liner to have right vertical alignment
                    [
                        sg.Column(
                            layout=[
                                [
                                    sg.Text(
                                        "Numero Tessera", size=TEXT_SIZE, font=FONT
                                    ),
                                    sg.Input(
                                        key=clk.input_card_id, size=(30, 1), font=FONT
                                    ),
                                ]
                            ]
                        ),
                        sg.Column(
                            layout=[
                                [
                                    sg.Button(
                                        "Cerca",
                                        key=clk.button_search_by_id,
                                        size=(7, 1),
                                        font=FONT,
                                    )
                                ]
                            ]
                        ),
                    ],
                    [
                        sg.Text(
                            "oppure effettua ricerca tramite Nome e Cognome:",
                            size=ALL_WINDOW_SIZE,
                            font=FONT,
                        )
                    ],
                    [
                        sg.Column(
                            layout=[
                                [
                                    sg.Text("Nome", size=TEXT_SIZE, font=FONT),
                                    sg.Input(
                                        key=clk.input_name_search,
                                        size=(30, 1),
                                        font=FONT,
                                    ),
                                ],
                                [
                                    sg.Text("Cognome", size=TEXT_SIZE, font=FONT),
                                    sg.Input(
                                        key=clk.input_surname_search,
                                        size=(30, 1),
                                        font=FONT,
                                    ),
                                ],
                            ]
                        ),
                        sg.Column(
                            layout=[
                                [
                                    sg.Button(
                                        "Cerca",
                                        key=clk.button_search_by_name,
                                        size=(7, 2),
                                        font=FONT,
                                    )
                                ]
                            ]
                        ),
                    ],
                ],
            )
        ],
        [
            sg.Text(
                "",
                key=clk.text_search_response,
                size=ALL_WINDOW_SIZE,
                font=FONT,
                text_color=ATTENTION_TEXT_COLOR,
            )
        ],
        [
            sg.Text("Nome", size=TEXT_SIZE, font=FONT),
            sg.Input(
                key=clk.input_name_insert, size=INPUT_SIZE, font=FONT, disabled=True
            ),
        ],
        [
            sg.Text("Cognome", size=TEXT_SIZE, font=FONT),
            sg.Input(
                key=clk.input_surname_insert, size=INPUT_SIZE, font=FONT, disabled=True
            ),
        ],
        calendar_input(
            text="Data attivazione servizio",
            input_key=clk.input_start_activation_date,
            input_enable_events=True,
            disabled=True,
        ),
        calendar_input(
            text="Data fine servizio",
            input_key=clk.input_end_activation_date,
            disabled=True,
        ),
        [
            sg.Text("Data ultimo accesso", size=TEXT_SIZE, font=FONT),
            sg.Input(
                "Nessuna",
                key=clk.output_last_access_date,
                size=(20, 1),
                font=FONT,
                disabled=True,
                justification="right",
            ),
        ],
        [
            sg.Text("Punti totali", font=FONT, justification="right"),
            sg.Input(
                key=clk.input_total_points,
                size=(5, 1),
                font=FONT,
                justification="right",
            ),
            sg.Text("", size=(7, 1)),
            sg.Text("Punti rimanenti", font=FONT),
            sg.Input(
                key=clk.output_remaining_points,
                size=(5, 1),
                font=FONT,
                justification="right",
            ),
        ],
        [
            sg.Text("Inviato da", size=TEXT_SIZE, font=FONT),
            sg.InputCombo(
                ch.sent_by_set,
                default_value="Scegli un'opzione dal menù o digita nuovo inviante",
                key=clk.input_sent_by,
                size=INPUT_SIZE,
                font=FONT,
            ),
        ],
        [
            sg.Frame(
                title="Ragione invio",
                font=FRAME_TITLE_FONT,
                title_color=FRAME_TITLE_COLOR,
                layout=[
                    [
                        sg.Checkbox(
                            "Difficoltà economiche",
                            key=clk.cb_motivation_sent_de,
                            size=(27, 1),
                            font=FONT,
                        ),
                        sg.Checkbox(
                            "Famiglia numerosa",
                            key=clk.cb_motivation_sent_fn,
                            size=(27, 1),
                            font=FONT,
                        ),
                    ],
                    [
                        sg.Checkbox(
                            "Disoccupazione",
                            key=clk.cb_motivation_sent_d,
                            size=(27, 1),
                            font=FONT,
                        ),
                        sg.Checkbox(
                            "Altro",
                            key=clk.cb_motivation_sent_other,
                            size=(27, 1),
                            font=FONT,
                        ),
                    ],
                ],
            )
        ],
        [
            sg.Frame(
                title="Altri servizi caritas",
                font=FRAME_TITLE_FONT,
                title_color=FRAME_TITLE_COLOR,
                layout=rearrange_list_in_columns(
                    [
                        sg.Checkbox(entry[0], key=entry[1], size=(27, 1), font=FONT)
                        for entry in zip(
                            ch.other_caritas_services_set, clk.cb_other_caritas_services
                        )
                    ],
                    2,
                ),
            )
        ],
        [
            sg.Button(
                "Salva nuova tessera",
                key=clk.button_save_card,
                size=(DOUBLE_COLUMN_WIDTH, 1),
                font=FONT,
                disabled=True,
            ),
            sg.Button(
                "Aggiorna",
                key=clk.button_update_card,
                size=(DOUBLE_COLUMN_WIDTH, 1),
                font=FONT,
                disabled=True,
            ),
        ],
        [
            sg.Button(
                "Rinnova tessera",
                key=clk.button_renew_card,
                size=(DOUBLE_COLUMN_WIDTH, 1),
                font=FONT,
                disabled=True,
            ),
            sg.Button(
                "Stampa",
                key=clk.button_print_card,
                size=(DOUBLE_COLUMN_WIDTH, 1),
                font=FONT,
                disabled=True,
            ),
        ],
    ]
