import pandas as pd

from src.data.headers.ticket_header import SummaryTicketTableHeader as stth


class GuiState:

    def __init__(self):
        self.tickets_loaded_card = None
        self.tickets_summary_ticket = pd.DataFrame(columns=stth.all)
        self.tickets_past_tickets = None

        self.anagraphic_current_insertion_family_id = None
        self.anagraphic_current_viewed_family = None
        self.anagraphic_current_viewed_family_id = None
        self.anagraphic_adding_new_component = False

        self.cards_current_id = None
