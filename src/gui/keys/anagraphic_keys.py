from src.gui.keys.constants import ANAGRAPHIC_KEY


class AnagraphicLayoutKeys:
    """ Keys for the elements in the anagraphic layout. """

    input_name_view = ANAGRAPHIC_KEY + "name_view"
    input_surname_view = ANAGRAPHIC_KEY + "surname_view"
    input_name_insert = ANAGRAPHIC_KEY + "name_insert"
    input_surname_insert = ANAGRAPHIC_KEY + "surname_insert"
    radio_sex_male = ANAGRAPHIC_KEY + "sex_male"
    radio_sex_female = ANAGRAPHIC_KEY + "sex_female"
    input_birth_date_view = ANAGRAPHIC_KEY + "birth_date_view"
    input_birth_date_insert = ANAGRAPHIC_KEY + "birth_date_insert"
    input_family_role = ANAGRAPHIC_KEY + "family_role"
    input_residence = ANAGRAPHIC_KEY + "residence"
    input_citizenship = ANAGRAPHIC_KEY + "citizenship"
    input_origin_country = ANAGRAPHIC_KEY + "country_of_origin"
    input_mobile = ANAGRAPHIC_KEY + "mobile"
    input_email = ANAGRAPHIC_KEY + "email"
    input_id_doc_type = ANAGRAPHIC_KEY + "id_document_type"
    input_id_doc_number = ANAGRAPHIC_KEY + "id_document_number"

    button_new_family = ANAGRAPHIC_KEY + "new_family"
    button_new_component = ANAGRAPHIC_KEY + "insert_new_component"
    button_save_family = ANAGRAPHIC_KEY + "save_family"
    button_search_family = ANAGRAPHIC_KEY + "search_family"
    button_add_component = ANAGRAPHIC_KEY + "add_component"
    button_save_updated_component = ANAGRAPHIC_KEY + "save_updated_component"
    button_modify_component = ANAGRAPHIC_KEY + "modify_component"
    button_bind_card = ANAGRAPHIC_KEY + "bind_card"
    button_delete_component = ANAGRAPHIC_KEY + "delete_component"

    text_search_response = ANAGRAPHIC_KEY + "search_response"
    text_search_name = ANAGRAPHIC_KEY + "search_name"
    text_search_surname = ANAGRAPHIC_KEY + "search_surname"
    text_family_insertion_response = ANAGRAPHIC_KEY + "family_insertion_response"

    table_family = ANAGRAPHIC_KEY + "table_family"

    tab_insert_family = ANAGRAPHIC_KEY + "tab_insert_family"
    tab_view_family = ANAGRAPHIC_KEY + "tab_view_family"

    output_mobile = ANAGRAPHIC_KEY + "capofamiglia_mobile"
    output_email = ANAGRAPHIC_KEY + "capofamiglia_email"
