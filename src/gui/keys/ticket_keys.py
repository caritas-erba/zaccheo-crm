from src.gui.keys.constants import TICKETS_KEY


class TicketsLayoutKeys:
    """ Defines keys for elements in ticket layout. """

    input_insert_card_id = TICKETS_KEY + "insert_card_id"
    input_view_card_id = TICKETS_KEY + "view_card_id"
    input_target = TICKETS_KEY + "target"
    input_category = TICKETS_KEY + "category"
    input_product = TICKETS_KEY + "product"
    input_quantity = TICKETS_KEY + "quantity"

    output_last_access_date = TICKETS_KEY + "last_access_date"
    output_insert_service_end_date = TICKETS_KEY + "insert_service_end_date"
    output_insert_available_points = TICKETS_KEY + "insert_available_points"
    output_ticket_summary_table = TICKETS_KEY + "ticket_summary_table"
    output_insert_ticket_total_points = TICKETS_KEY + "insert_ticket_total_points"
    output_past_ticket_summary_table = TICKETS_KEY + "past_ticket_summary_table"
    output_past_ticket_dates_list = TICKETS_KEY + "past_tickets_dates_list"
    output_view_service_start_date = TICKETS_KEY + "view_service_start_date"
    output_view_service_end_date = TICKETS_KEY + "view_service_end_date"
    output_view_total_points_spent = TICKETS_KEY + "total_points_spent"
    output_view_available_points = TICKETS_KEY + "view_available_points"
    output_view_ticket_total_points = TICKETS_KEY + "view_ticket_total_points"

    button_insert_find_card = TICKETS_KEY + "insert_find_card"
    button_view_find_card = TICKETS_KEY + "view_find_card"
    button_new_ticket = TICKETS_KEY + "new_ticket"
    button_add_article = TICKETS_KEY + "add_article"
    button_remove_article = TICKETS_KEY + "remove_article"
    button_save_and_print_ticket = TICKETS_KEY + "save_ticket"

    def __init__(self):
        pass
