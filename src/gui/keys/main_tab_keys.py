from src.gui.keys.constants import MAIN_TAB_KEYS


class MainTabKeys:

    anagraphic_tab = MAIN_TAB_KEYS + "anagraphic"
    card_keys = MAIN_TAB_KEYS + "cards"
    product_keys = MAIN_TAB_KEYS + "products"
    ticker_keys = MAIN_TAB_KEYS + "tickets"
