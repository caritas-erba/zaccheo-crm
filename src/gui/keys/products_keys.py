from src.gui.keys.constants import PRODUCT_KEY


class ProductsLayoutKeys:

    select_category = PRODUCT_KEY + "categoria"
    select_target = PRODUCT_KEY + "destinatario"
    select_product = PRODUCT_KEY + "select_prodotto"
    input_product = PRODUCT_KEY + "input_prodotto"
    input_description = PRODUCT_KEY + "descrizione"
    input_points = PRODUCT_KEY + "punti"
    input_max_purchasable = PRODUCT_KEY + "max_acquistabili"

    button_create_new = PRODUCT_KEY + "create_new"
    button_save = PRODUCT_KEY + "save"
    button_find = PRODUCT_KEY + "find"
    button_update = PRODUCT_KEY + "update"
    button_delete = PRODUCT_KEY + "delete"

    def __init__(self):
        pass
