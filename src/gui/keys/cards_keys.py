from src.gui.keys.constants import CARDS_KEY
from src.data.headers.cards_header import CardsHeader as ch
from src.config import get_env, EnvironmentalVariableNames as EnvVar


class CardsLayoutKeys:

    input_card_id = CARDS_KEY + "card_id"
    input_name_insert = CARDS_KEY + "name_insert"
    input_name_search = CARDS_KEY + "name_search"
    input_surname_insert = CARDS_KEY + "surname_insert"
    input_surname_search = CARDS_KEY + "surname_search"
    input_start_activation_date = CARDS_KEY + "start_activation_date"
    input_end_activation_date = CARDS_KEY + "end_activation_date"
    input_total_points = CARDS_KEY + "total_points"
    input_sent_by = CARDS_KEY + "sent_by"
    cb_motivation_sent_de = CARDS_KEY + "motivation_sent_difficolta_economica"
    cb_motivation_sent_fn = CARDS_KEY + "motivation_sent_famiglia_numerosa"
    cb_motivation_sent_d = CARDS_KEY + "motivation_sent_disoccupazione"
    cb_motivation_sent_other = CARDS_KEY + "motivation_sent_other"
    cb_other_caritas_services_emp = CARDS_KEY + "other_caritas_emporio_alimentare"
    cb_other_caritas_services_pd = CARDS_KEY + "other_caritas_progetto_decima"
    cb_other_caritas_services_emab = CARDS_KEY + "other_caritas_emergenza_abitativa"
    cb_other_caritas_services_cas = CARDS_KEY + "other_caritas_cas"
    cb_other_caritas_services_other = CARDS_KEY + "other_caritas_other"

    text_search_response = CARDS_KEY + "search_response"

    output_last_access_date = CARDS_KEY + "last_access_date"
    output_remaining_points = CARDS_KEY + "remaining_points"

    button_search_by_id = CARDS_KEY + "search_by_id"
    button_search_by_name = CARDS_KEY + "search_by_name"
    button_save_card = CARDS_KEY + "save"
    button_update_card = CARDS_KEY + "update"
    button_print_card = CARDS_KEY + "print"
    button_renew_card = CARDS_KEY + "renew"

    cb_motivation_sent = [
        cb_motivation_sent_de,
        cb_motivation_sent_fn,
        cb_motivation_sent_d,
        cb_motivation_sent_other,
    ]
    cb_other_caritas_services = (
        [
            cb_other_caritas_services_emp,
            cb_other_caritas_services_pd,
            cb_other_caritas_services_emab,
            cb_other_caritas_services_cas,
            cb_other_caritas_services_other,
        ]
        if get_env(EnvVar.ZACCHEO_CARITAS_GROUP) == "erba"
        else [
            cb_other_caritas_services_emab,
            cb_other_caritas_services_cas,
            cb_other_caritas_services_other,
        ]
    )

    all_event_keys = [
        button_search_by_id,
        button_save_card,
        input_total_points,
        button_update_card,
        input_start_activation_date,
        button_search_by_name,
        button_print_card,
    ]

    @staticmethod
    def map_cb_motivation_sent_keys2values():
        return {
            CardsLayoutKeys.cb_motivation_sent[i]: ch.motivation_sent_set[i]
            for i in range(0, len(CardsLayoutKeys.cb_motivation_sent))
        }

    @staticmethod
    def map_cb_other_caritas_services_keys2values():
        return {
            CardsLayoutKeys.cb_other_caritas_services[i]: ch.other_caritas_services_set[
                i
            ]
            for i in range(0, len(CardsLayoutKeys.cb_other_caritas_services))
        }
