import logging


def parse_text_datum_insert(datum):
    """
    Parses a text datum to be inserted into db.

    Performs:
    - casting to str if not already
    - strip
    - uppercase

    @param datum:   The text datum to be parsed
    @return:        The parsed datum
    """
    try:
        datum = str(datum)
        datum = (
            datum
            .strip()
            .upper()
            # .replace("'", "''")
        )
        return datum
    except TypeError as te:
        logging.error(f"Datum can not be casted to string: {te}")
    except Exception as e:
        logging.error(f"{e}")


def parse_text_datum_query(datum):
    """
    Parses a text datum to be queried from db.

    Performs:
    - casting to str if not already
    - strip
    - uppercase
    - doubling ' to escape ' if present

    @param datum:   The text datum to be parsed
    @return:        The parsed datum
    """
    try:
        datum = str(datum)
        datum = (
            datum
            .strip()
            .upper()
            .replace("'", "''")
        )
        return datum
    except TypeError as te:
        logging.error(f"Datum can not be casted to string: {te}")
    except Exception as e:
        logging.error(f"{e}")
