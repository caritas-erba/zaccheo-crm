import datetime
import logging

from src.gui.layouts.constants import CALENDAR_DATE_FORMAT


def is_valid_date_format(date: str):
    """
    Checks whether the provided date string has the correct format.

    @param date:    Date
    @return:        True if the format is the same as the defined default format
    """
    try:
        datetime.datetime.strptime(date, CALENDAR_DATE_FORMAT)
        return True
    except ValueError as e:
        logging.error(e)
        return False


def enforce_2digits_date_format(date: str):
    """
    Converts a valid date string into a date string with always two digits for months and days.

    @param date:
    @return:
    """
    return datetime.datetime.strptime(date.strip(), CALENDAR_DATE_FORMAT).strftime(CALENDAR_DATE_FORMAT)

