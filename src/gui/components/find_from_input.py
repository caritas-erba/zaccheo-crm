import PySimpleGUI as sg

from src.gui.layouts.constants import FONT, TEXT_SIZE


def find_from_input_with_text(text, input_key, button_find_key):
    """
    Displays a text with an input field nearby it.

    @return:
    """
    return [
        sg.Text(text, size=TEXT_SIZE, font=FONT),
        sg.Input(key=input_key, size=(27, 1), font=FONT),
        sg.Button("Cerca", key=button_find_key, size=(10, 1), font=FONT)
    ]
