import PySimpleGUI as sg

from src.gui.layouts.constants import FONT, TEXT_SIZE, INPUT_SIZE


def input_with_text(text, input_key):
    """
    Displays a text with an input field nearby it.

    @return:
    """
    return [
        sg.Text(text, size=TEXT_SIZE, font=FONT),
        sg.Input(key=input_key, size=INPUT_SIZE, font=FONT),
    ]
