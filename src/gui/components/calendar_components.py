import PySimpleGUI as sg

from src.gui.layouts.constants import (CALENDAR_DATE_FORMAT, CALENDAR_DAY_NAMES, CALENDAR_MONTH_NAMES,
                                       CALENDAR_DATE_DEFAULT, FONT, TEXT_SIZE)


def calendar_input(text, input_key, input_enable_events=False, input_justification="right", disabled=False):
    return [
        sg.Text(text, size=TEXT_SIZE, font=FONT),
        sg.Input(default_text=CALENDAR_DATE_DEFAULT, key=input_key, size=(20, 1), font=FONT,
                 enable_events=input_enable_events, justification=input_justification, disabled=disabled),
        sg.CalendarButton("Calendario", size=(16, 1), font=FONT, format=CALENDAR_DATE_FORMAT,
                          begin_at_sunday_plus=1, month_names=CALENDAR_MONTH_NAMES,
                          day_abbreviations=CALENDAR_DAY_NAMES)
    ]
