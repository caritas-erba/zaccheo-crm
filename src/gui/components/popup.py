import os

import PySimpleGUI as sg

from src.config import get_env, EnvironmentalVariableNames as EnvVar
from src.gui.layouts.constants import (
    FONT,
    POPUP_WARNING_TITLE,
    POPUP_SUCCESS_TITLE,
    POPUP_ERROR_TITLE,
)


def warning_popup(text):
    return sg.popup_ok(
        text,
        title=POPUP_WARNING_TITLE,
        font=FONT,
        icon=os.path.join(
            get_env(EnvVar.ZACCHEO_WORKING_DIR), "resources", "caritas-logo.ico"
        ),
    )


def success_popup(text):
    return sg.popup_ok(
        text,
        title=POPUP_SUCCESS_TITLE,
        font=FONT,
        icon=os.path.join(
            get_env(EnvVar.ZACCHEO_WORKING_DIR), "resources", "caritas-logo.ico"
        ),
    )


def error_popup(text):
    return sg.popup_ok(
        text,
        title=POPUP_ERROR_TITLE,
        font=FONT,
        icon=os.path.join(
            get_env(EnvVar.ZACCHEO_WORKING_DIR), "resources", "caritas-logo.ico"
        ),
    )
