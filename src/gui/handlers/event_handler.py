import logging
from datetime import timedelta, datetime
import os
import sys
import subprocess
from copy import copy

import pandas as pd
import PySimpleGUI as sg
from fpdf import FPDF

from src.config import get_env, EnvironmentalVariableNames as EnvVar
from src.gui.keys.products_keys import ProductsLayoutKeys as plk
from src.gui.keys.anagraphic_keys import AnagraphicLayoutKeys as alk
from src.gui.keys.cards_keys import CardsLayoutKeys as clk
from src.gui.keys.main_tab_keys import MainTabKeys as mtk
from src.gui.keys.ticket_keys import TicketsLayoutKeys as tlk
from src.data.headers.products_header import ProductsHeader as ph
from src.data.headers.anagraphic_header import (
    FamilyTableHeader as fth,
    AnagraphicHeader as ah,
)
from src.data.headers.cards_header import CardsHeader as ch
from src.data.headers.ticket_header import (
    SummaryTicketTableHeader as stth,
    TicketsHeader as th,
)
from src.gui.layouts.constants import CALENDAR_DATE_DEFAULT, CALENDAR_DATE_FORMAT
from src.gui.utils.format import is_valid_date_format, enforce_2digits_date_format
from src.gui.components.popup import warning_popup, success_popup
from src.gui.state.state import GuiState
from src.controller import products_controller
from src.controller import anagraphic_controller
from src.controller import cards_controller
from src.controller import tickets_controller
from src.utils.time import compute_age, today_to_str
from src.constants import PRINTED_CARDS_FOLDER_PATH
from src.gui.handlers.utils.cards import (
    compute_card_points,
    points_to_update,
    is_card_data_quality_ok,
    store_motivation_sent,
    store_other_caritas_services,
)


class EventHandler:
    def __init__(self, window: sg.Window):
        self._window = window
        self._state = GuiState()

    def handle(self, event, values):
        if event == plk.select_target or event == plk.select_category:
            self._products_retrieve_products(values)
        elif event == plk.select_product:
            self._products_show_product(values)
        elif event == plk.button_create_new:
            self._products_create_new(values)
        elif event == plk.button_save:
            self._products_save(values)
        elif event == plk.button_update:
            self._products_update(values)
        elif event == plk.button_delete:
            self._products_delete(values)
        elif event == alk.button_new_family:
            self._enable_family_insertion()
        elif event == alk.button_new_component:
            self._anagraphic_new_family_component(values)
        elif event == alk.button_save_family:
            self._anagraphic_save_family(values)
        elif event == alk.button_search_family:
            self._anagraphic_search_family(values)
        elif event == alk.button_add_component:
            self._anagraphic_add_family_component()
        elif event == alk.button_modify_component:
            self._anagraphic_modify_component(values)
        elif event == alk.button_delete_component:
            self._anagraphic_delete_component(values)
        elif event == alk.button_save_updated_component:
            self._anagraphic_update_component(values)
        elif event == alk.button_bind_card:
            self._anagraphic_bind_card()
        elif event == clk.button_search_by_id:
            self._cards_search_by_id(values)
        elif event == clk.button_search_by_name:
            self._cards_search_by_name(values)
        elif event == clk.button_save_card:
            self._cards_save(values)
        elif event == clk.button_renew_card:
            self._cards_renew_card(values)
        elif event == clk.input_start_activation_date:
            self._cards_update_end_activation_date(values)
        elif event == clk.button_update_card:
            self._cards_update(values)
        elif event == clk.button_print_card:
            self._cards_print_card(values)
        elif event == tlk.button_insert_find_card:
            self._tickets_insert_find_card(values)
        elif event == tlk.button_new_ticket:
            self._tickets_new_ticket()
        elif event == tlk.input_category or event == tlk.input_target:
            self._tickets_retrieve_products(values)
        elif event == tlk.input_quantity:
            self._tickets_enable_add_article(values)
        elif event == tlk.button_add_article:
            self._tickets_add_article(values)
        elif event == tlk.button_save_and_print_ticket:
            self._tickets_save_and_print_ticket(values)
        elif event == tlk.button_remove_article:
            self._tickets_remove_article(values)
        elif event == tlk.button_view_find_card:
            self._tickets_view_find_card(values)
        elif event == tlk.output_past_ticket_dates_list:
            self._tickets_show_ticket(values)
        else:
            logging.error("Very bad. Event not handled.")
            return

    def _products_clean_fields(self, disabled):
        self._window[plk.input_description].update("", disabled=disabled)
        self._window[plk.input_points].update("", disabled=disabled)
        self._window[plk.input_max_purchasable].update("", disabled=disabled)
        self._window[plk.input_product].update("", disabled=disabled)
        return

    def _products_create_new(self, values):
        """
        Enables the Save button for a new product
        @return:
        """
        if len(values[plk.select_target]) == 0 or len(values[plk.select_category]) == 0:
            warning_popup(
                "Selezionare 'Categoria' e 'Prodotto' per poter creare un nuovo prodotto."
            )
        else:
            self._products_clean_fields(disabled=False)
            self._window[plk.button_save].update(disabled=False)
            self._window[plk.button_update].update(disabled=True)
            self._window[plk.button_delete].update(disabled=True)
        return

    def _products_save(self, values):
        if (
            values[plk.select_product] == ""
            or values[plk.select_category] == []
            or values[plk.select_target] == []
            or values[plk.input_product].strip() == ""
        ):
            warning_popup("Inserire i dati per poter salvare il prodotto.")
            return

        if (
            not values[plk.input_points].strip().isdigit()
            or not values[plk.input_max_purchasable].strip().isdigit()
        ):
            if (
                len(values[plk.input_points]) == 0
                or len(values[plk.input_max_purchasable]) == 0
            ):
                warning_popup(
                    "Uno o entrambi i campi 'Punti' e 'Max acquistabili' sono vuoti."
                )
            else:
                warning_popup("Lettere presenti nei campi 'Punti' e 'Max acquistabili'")
            return

        products_controller.create_new_product(
            product=values[plk.input_product].upper().strip(),
            category=values[plk.select_category][0],
            target=values[plk.select_target][0],
            description=values[plk.input_description].strip(),
            points=values[plk.input_points].strip(),
            max_purchasable=values[plk.input_max_purchasable].strip(),
        )
        # Cleaning fields
        self._products_clean_fields(disabled=True)
        self._window[plk.button_save].update(disabled=True)
        self._window[plk.button_update].update(disabled=True)
        self._window[plk.select_product].set_value([])

        # Update shown values in the selection product listbox
        shown_values = self._window[plk.select_product].get_list_values()
        self._window[plk.select_product].update(
            sorted(shown_values + [values[plk.input_product].upper().strip()])
        )

        logging.info(f"Inserted new product {values[plk.input_product].upper()}")
        success_popup("Prodotto salvato correttamente!")
        return

    def _products_retrieve_products(self, values):
        """
        Looks for available products of the selected category and target.

        @param values:
        @return:
        """
        if len(values[plk.select_category]) == 0 or len(values[plk.select_target]) == 0:
            return
        else:
            # Clean product fields if they are showing something
            self._products_clean_fields(disabled=True)
            self._window[plk.button_save].update(disabled=True)
            self._window[plk.button_update].update(disabled=True)
            self._window[plk.button_delete].update(disabled=True)

            # Retrieve available products
            products = products_controller.find_products(
                category=values[plk.select_category][0],
                target=values[plk.select_target][0],
            )
            self._window[plk.select_product].update(products)
            return

    def _products_show_product(self, values):
        if len(values[plk.select_product]) > 0:
            logging.info(
                f"Showing product {values[plk.select_product][0]} of category {values[plk.select_category][0]} "
                f"and target {values[plk.select_category][0]}"
            )

            product = products_controller.find_product(
                product_name=values[plk.select_product][0],
                target=values[plk.select_target][0],
                category=values[plk.select_category][0],
            )

            # Showing product info and allowing to modify
            self._window[plk.input_product].update(product[ph.product], disabled=False)
            self._window[plk.input_description].update(
                product[ph.description], disabled=False
            )
            self._window[plk.input_points].update(product[ph.points], disabled=False)
            self._window[plk.input_max_purchasable].update(
                product[ph.max_purchasable], disabled=False
            )

            # Enable buttons to update and delete the product
            self._window[plk.button_update].update(disabled=False)
            self._window[plk.button_delete].update(disabled=False)
            self._window[plk.button_save].update(disabled=True)

        return

    def _products_update(self, values):
        """
        Updates product data.

        @param values:
        @return:
        """

        # Check if input are valid
        if (
            not values[plk.input_points].strip().isdigit()
            or not values[plk.input_max_purchasable].strip().isdigit()
        ):
            if (
                len(values[plk.input_points]) == 0
                or len(values[plk.input_max_purchasable]) == 0
            ):
                warning_popup(
                    "Uno o entrambi i campi 'Punti' e 'Max acquistabili' sono vuoti."
                )
            else:
                warning_popup("Lettere presenti nei campi 'Punti' e 'Max acquistabili'")
            return

        old_product_name = values[plk.select_product][0]
        new_product_name = values[plk.input_product].upper().strip()

        products_controller.update_product(
            old_product_name=old_product_name,
            new_product_name=new_product_name,
            category=values[plk.select_category][0],
            target=values[plk.select_target][0],
            description=values[plk.input_description].strip(),
            points=values[plk.input_points].strip(),
            max_purchasable=values[plk.input_max_purchasable].strip(),
        )

        # Updating shown products
        shown_products = self._window[plk.select_product].get_list_values()
        shown_products[shown_products.index(old_product_name)] = new_product_name
        self._window[plk.select_product].update(shown_products)

        # Cleaning fields
        self._products_clean_fields(disabled=True)
        self._window[plk.button_update].update(disabled=True)
        self._window[plk.button_delete].update(disabled=True)

        success_popup("Prodotto aggiornato correttamente!")

    def _products_delete(self, values):
        """
        Deletes the selected product.

        @param values:
        @return:
        """
        products_controller.delete_product(
            product_name=values[plk.select_product][0],
            target=values[plk.select_target][0],
            category=values[plk.select_category][0],
        )

        # Cleaning fields
        self._products_clean_fields(disabled=True)
        self._window[plk.button_update].update(disabled=True)
        self._window[plk.button_delete].update(disabled=True)
        # Update shown values in the product selection listbox
        shown_products = self._window[plk.select_product].get_list_values()
        shown_products.remove(values[plk.select_product][0])
        self._window[plk.select_product].update(shown_products)

        success_popup("Prodotto eliminato!")

    def _change_anagraphic_insertion_form_state(self, disabled):
        """

        @param disabled:    Boolean. If True, all inputs are disabled and no new family can be inserted
        @return:
        """
        self._window[alk.input_name_insert].update(value="", disabled=disabled)
        self._window[alk.input_surname_insert].update(value="", disabled=disabled)
        self._window[alk.radio_sex_male].update(value=False, disabled=disabled)
        self._window[alk.radio_sex_female].update(value=False, disabled=disabled)
        self._window[alk.input_birth_date_insert].update(
            value=CALENDAR_DATE_DEFAULT, disabled=disabled
        )
        self._window[alk.input_family_role].update(
            set_to_index=ah.family_role_set.index("Capofamiglia"), disabled=disabled
        )
        self._window[alk.input_residence].update(value="", disabled=disabled)
        self._window[alk.input_citizenship].update(value="", disabled=disabled)
        self._window[alk.input_origin_country].update(value="", disabled=disabled)
        self._window[alk.input_mobile].update(value="", disabled=disabled)
        self._window[alk.input_email].update(value="", disabled=disabled)
        self._window[alk.input_id_doc_type].update(
            set_to_index=ah.id_document_type_set.index("Nessuno"), disabled=disabled
        )
        self._window[alk.input_id_doc_number].update(value="", disabled=disabled)

        # Allowing the insertion of a new family or family component
        self._window[alk.button_save_updated_component].update(disabled=True)
        self._window[alk.button_new_component].update(disabled=disabled)
        self._window[alk.button_save_family].update(disabled=disabled)
        return

    def _enable_family_insertion(self):
        self._change_anagraphic_insertion_form_state(disabled=False)
        return

    def _anagraphic_new_family_component(self, values):
        # Warning message if name or surname are missing
        if (
            values[alk.input_name_insert] == ""
            or values[alk.input_surname_insert] == ""
            or (values[alk.radio_sex_male] and values[alk.radio_sex_female])
            or values[alk.input_birth_date_insert] == CALENDAR_DATE_DEFAULT
        ):
            warning_popup(
                "Inserire almeno Nome, Cognome, Sesso e \n"
                "Data di Nascita per salvare la persona."
            )
            return

        # Getting right family id
        if self._state.anagraphic_current_insertion_family_id is None:
            self._state.anagraphic_current_insertion_family_id = (
                anagraphic_controller.get_last_family_id() + 1
            )

        # Checking date format
        if not is_valid_date_format(values[alk.input_birth_date_insert].strip()):
            warning_popup(
                "La data di nascita ha un formato sbagliato.\n"
                "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
            )
            return

        # Check if person is already present in db
        family_id = anagraphic_controller.get_person_family_id(
            values[alk.input_name_insert],
            values[alk.input_surname_insert],
            values[alk.input_birth_date_insert],
        )
        if family_id is not None:
            warning_popup(
                f"Questa persona è già presente nei dati. Utilizzare la scheda "
                f"'Ricerca famiglia' per visualizzare la sua famiglia. La tessera "
                f"loro collegata è la n. {family_id}"
            )
            self._change_anagraphic_insertion_form_state(disabled=True)
            return

        # Saving person
        anagraphic_controller.save_family_component(
            family_id=self._state.anagraphic_current_insertion_family_id,
            name=values[alk.input_name_insert],
            surname=values[alk.input_surname_insert],
            sex="M" if values[alk.radio_sex_male] else "F",
            birth_date=enforce_2digits_date_format(values[alk.input_birth_date_insert]),
            family_role=values[alk.input_family_role][0],
            residence=values[alk.input_residence],
            citizenship=values[alk.input_citizenship],
            origin_country=values[alk.input_origin_country],
            mobile=values[alk.input_mobile],
            email=values[alk.input_email],
            id_doc_type=values[alk.input_id_doc_type][0],
            id_doc_number=values[alk.input_id_doc_number],
        )

        # Update window field
        self._window[alk.input_name_insert].update("")
        self._window[alk.radio_sex_male].update(False)
        self._window[alk.radio_sex_female].update(False)
        self._window[alk.input_birth_date_insert].update(CALENDAR_DATE_DEFAULT)
        self._window[alk.input_mobile].update("")
        self._window[alk.input_email].update("")
        self._window[alk.input_id_doc_number].update("")

        logging.info(
            f"Inserted new person {values[alk.input_name_insert]} "
            f"{values[alk.input_surname_insert]}"
        )
        success_popup("Componente inserito correttamente!")
        return

    def _anagraphic_save_family(self, values):
        # Warning message if name or surname are missing
        if (
            values[alk.input_name_insert] == ""
            or values[alk.input_surname_insert] == ""
            or (values[alk.radio_sex_male] and values[alk.radio_sex_female])
            or values[alk.input_birth_date_insert] == CALENDAR_DATE_DEFAULT
        ):
            warning_popup(
                "Inserire almeno Nome, Cognome, Sesso e \n"
                "Data di Nascita salvare la persona"
            )
            return

        # Getting right family id if family has only one component
        if self._state.anagraphic_current_insertion_family_id is None:
            self._state.anagraphic_current_insertion_family_id = (
                anagraphic_controller.get_last_family_id() + 1
            )

        # Checking date format
        if not is_valid_date_format(values[alk.input_birth_date_insert].strip()):
            warning_popup(
                "La data di nascita ha un formato sbagliato.\n"
                "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
            )
            return

        # Check if person is already present in db
        family_id = anagraphic_controller.get_person_family_id(
            values[alk.input_name_insert],
            values[alk.input_surname_insert],
            values[alk.input_birth_date_insert],
        )
        if family_id is not None:
            warning_popup(
                f"Questa persona è già presente nei dati. Utilizzare la scheda "
                f"'Ricerca famiglia' per visualizzare la sua famiglia. La tessera "
                f"loro collegata è la n. {family_id}"
            )
            self._change_anagraphic_insertion_form_state(disabled=True)
            return

        anagraphic_controller.save_family_component(
            family_id=self._state.anagraphic_current_insertion_family_id,
            name=values[alk.input_name_insert],
            surname=values[alk.input_surname_insert],
            sex="M" if values[alk.radio_sex_male] else "F",
            birth_date=enforce_2digits_date_format(values[alk.input_birth_date_insert]),
            family_role=values[alk.input_family_role][0],
            residence=values[alk.input_residence],
            citizenship=values[alk.input_citizenship],
            origin_country=values[alk.input_origin_country],
            mobile=values[alk.input_mobile],
            email=values[alk.input_email],
            id_doc_type=values[alk.input_id_doc_type][0],
            id_doc_number=values[alk.input_id_doc_number],
        )

        logging.info(
            f"Inserted new person {values[alk.input_name_insert]} "
            f"{values[alk.input_surname_insert]} with family id "
            f"{self._state.anagraphic_current_insertion_family_id}"
        )
        if self._state.anagraphic_adding_new_component:
            # Updating points related to already existent card
            card = cards_controller.search_card_by_id(
                card_id=self._state.anagraphic_current_insertion_family_id
            )
            points_to_add = points_to_update(card, values[alk.input_birth_date_insert])

            cards_controller.update_card(
                card_id=self._state.anagraphic_current_insertion_family_id,
                name=card[ch.name],
                surname=card[ch.surname],
                total_points=card[ch.total_points] + points_to_add,
                remaining_points=card[ch.current_points] + points_to_add,
                service_start_date=card[ch.service_start_date],
                service_end_date=card[ch.service_end_date],
                last_access_date=card[ch.last_access_date],
                sent_by=card[ch.sent_by],
                motivation_sent=card[ch.motivation_sent],
                other_caritas_services=card[ch.other_caritas_services],
            )

            # Updating state
            self._state.anagraphic_adding_new_component = False
            success_popup("Nuovo famigliare aggiunto correttamente!")
        else:
            success_popup("Nuova famiglia inserita correttamente!")

        # Setting family id to None to allow insertion of new family
        self._state.anagraphic_current_insertion_family_id = None

        # Clear fields
        self._change_anagraphic_insertion_form_state(disabled=True)
        return

    def _anagraphic_search_family(self, values):
        if (
            values[alk.input_name_view] == ""
            or values[alk.input_surname_view] == ""
            or values[alk.input_birth_date_view] == CALENDAR_DATE_DEFAULT
        ):
            warning_popup(
                "Inserire Nome, Cognome e Data di Nascita per poter effettuare la ricerca"
            )
            return

        # Checking date format
        if not is_valid_date_format(values[alk.input_birth_date_view].strip()):
            warning_popup(
                "La data di nascita ha un formato sbagliato.\n"
                "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
            )
            return

        name = values[alk.input_name_view].upper().strip()
        surname = values[alk.input_surname_view].upper().strip()
        birth_date = enforce_2digits_date_format(values[alk.input_birth_date_view])
        logging.info(
            f"Querying for family of {name} {surname}, birth date {birth_date}"
        )

        # Getting family data
        family_id = anagraphic_controller.get_person_family_id(
            name, surname, birth_date
        )
        if family_id is None:
            logging.info("No corresponding family found")
            self._window[alk.text_search_response].update(
                "Persona non presente nel database. " "Effettuare una nuova ricerca."
            )
            self._window[alk.button_modify_component].update(disabled=True)
            self._window[alk.button_add_component].update(disabled=True)
            self._window[alk.button_bind_card].update(disabled=True)
            self._window[alk.button_delete_component].update(disabled=True)
            self._window[alk.table_family].update(
                values=[["" for row in range(0, 10)] for col in range(0, 5)]
            )
        else:
            family = anagraphic_controller.get_family(family_id)
            logging.info(f"Found family of {family.shape[0]} component")

            # Computing age and selecting data to show
            family_table = family
            family_table.loc[:, fth.age] = family_table[ah.birth_date].apply(
                compute_age
            )
            self._state.anagraphic_current_viewed_family = family_table
            self._state.anagraphic_current_viewed_family_id = family_table[
                ah.family_id
            ].unique()[0]
            family_table = family_table.rename(columns=fth.map_from_anagraphic())[
                fth.all
            ]

            # Updating table with data
            self._window[alk.text_search_response].update(
                "Famiglia trovata. Per visualizzare e modificare i dati "
                "di un componente, selezionarlo prima nella tabella."
            )
            self._window[alk.table_family].update(values=family_table.values.tolist())
            self._window[alk.button_modify_component].update(disabled=False)
            self._window[alk.button_add_component].update(disabled=False)
            self._window[alk.button_bind_card].update(disabled=False)
            self._window[alk.button_delete_component].update(disabled=False)

            # Updating contact of capofamiglia
            capofamiglia = family[family[ah.family_role] == "Capofamiglia"]
            self._window[alk.output_mobile].update(capofamiglia[ah.mobile].values[0])
            self._window[alk.output_email].update(capofamiglia[ah.email].values[0])

            # Check: if there is more than one Capofamiglia, notify to update the family role of the components.
            if family[family[ah.family_role] == "Capofamiglia"].shape[0] > 1:
                warning_popup(
                    "Presente più di un capofamiglia! Aggiornare il ruolo\n"
                    "dei famigliari per avere un unico capofamiglia.\n"
                    "Una volta fatto, eseguire nuovamente la ricerca."
                )
                self._window[alk.button_bind_card].update(disabled=True)
        return

    def _anagraphic_add_family_component(self):
        """
        Adds a new component to the viewed family.

        @return:
        """

        # Update state of anagraphic: we are adding a new component
        self._state.anagraphic_adding_new_component = True

        # Go to insertion tab and precompile fields
        self._window[alk.tab_insert_family].select()

        capofamiglia_data = self._state.anagraphic_current_viewed_family.loc[
            self._state.anagraphic_current_viewed_family[ah.family_role]
            == "Capofamiglia",
            [ah.surname, ah.residence, ah.citizenship, ah.origin_country],
        ].squeeze()

        self._window[alk.input_name_insert].update(value="", disabled=False)
        self._window[alk.input_surname_insert].update(
            value=capofamiglia_data[ah.surname], disabled=False
        )
        self._window[alk.radio_sex_male].update(value=False, disabled=False)
        self._window[alk.radio_sex_female].update(value=False, disabled=False)
        self._window[alk.input_birth_date_insert].update(
            value=CALENDAR_DATE_DEFAULT, disabled=False
        )
        self._window[alk.input_family_role].update(set_to_index=1, disabled=False)
        self._window[alk.input_residence].update(
            value=capofamiglia_data[ah.residence], disabled=False
        )
        self._window[alk.input_citizenship].update(
            value=capofamiglia_data[ah.citizenship], disabled=False
        )
        self._window[alk.input_origin_country].update(
            value=capofamiglia_data[ah.origin_country], disabled=False
        )
        self._window[alk.input_mobile].update(value="", disabled=False)
        self._window[alk.input_email].update(value="", disabled=False)
        self._window[alk.input_id_doc_type].update(
            set_to_index=ah.id_document_type_set.index("Nessuno"), disabled=False
        )
        self._window[alk.input_id_doc_number].update(value="", disabled=False)

        # Allowing the insertion of a new family or family component
        self._window[alk.button_save_updated_component].update(disabled=True)
        self._window[alk.button_new_component].update(disabled=True)
        self._window[alk.button_save_family].update(disabled=False)

        # Update the insertion_family_id to insert in correct family
        self._state.anagraphic_current_insertion_family_id = str(
            self._state.anagraphic_current_viewed_family_id
        ).strip()
        return

    def _anagraphic_modify_component(self, values):
        if len(values[alk.table_family]) == 0:
            warning_popup("Selezionare prima una persona nella tabella")
            return

        family_component = self._state.anagraphic_current_viewed_family.iloc[
            values[alk.table_family]
        ].squeeze()
        self._state.anagraphic_current_viewed_family_id = family_component[ah.family_id]
        logging.info(
            f"Modifying family component {family_component[ah.name]} {family_component[ah.surname]}"
        )

        # Selecting anagraphic insert tab to show data
        self._window[alk.tab_insert_family].select()
        self._window[alk.button_new_component].update(disabled=True)
        self._window[alk.button_save_family].update(disabled=True)
        self._window[alk.button_save_updated_component].update(disabled=False)

        # Updating fields according to data
        self._window[alk.input_name_insert].update(
            value=family_component[ah.name], disabled=False
        )
        self._window[alk.input_surname_insert].update(
            value=family_component[ah.surname], disabled=False
        )
        if family_component[ah.sex] == "M":
            self._window[alk.radio_sex_male].update(value=True, disabled=False)
        else:
            self._window[alk.radio_sex_female].update(value=True, disabled=False)
        self._window[alk.input_birth_date_insert].update(
            value=family_component[ah.birth_date], disabled=False
        )
        self._window[alk.input_family_role].update(
            set_to_index=ah.family_role_set.index(family_component[ah.family_role]),
            disabled=False,
        )
        self._window[alk.input_residence].update(
            value=family_component[ah.residence], disabled=False
        )
        self._window[alk.input_citizenship].update(
            value=family_component[ah.citizenship], disabled=False
        )
        self._window[alk.input_origin_country].update(
            value=family_component[ah.origin_country], disabled=False
        )
        self._window[alk.input_mobile].update(
            value=family_component[ah.mobile], disabled=False
        )
        self._window[alk.input_email].update(
            value=family_component[ah.email], disabled=False
        )
        self._window[alk.input_id_doc_type].update(
            set_to_index=ah.id_document_type_set.index(
                family_component[ah.id_document_type]
            ),
            disabled=False,
        )
        self._window[alk.input_id_doc_number].update(
            value=family_component[ah.id_document_number], disabled=False
        )
        return

    def _anagraphic_delete_component(self, values):
        if len(values[alk.table_family]) == 0:
            warning_popup("Selezionare prima una persona nella tabella")
            return

        component_data = self._state.anagraphic_current_viewed_family.loc[
            values[alk.table_family][0], [ah.name, ah.surname, ah.birth_date]
        ].squeeze()

        anagraphic_controller.delete_component(
            name=component_data[ah.name],
            surname=component_data[ah.surname],
            family_id=self._state.anagraphic_current_viewed_family_id,
        )

        # Update points of related card
        card = cards_controller.search_card_by_id(
            card_id=self._state.anagraphic_current_viewed_family_id
        )
        if not card.empty:
            points_to_remove = points_to_update(card, component_data[ah.birth_date])
            cards_controller.update_card(
                card_id=self._state.anagraphic_current_viewed_family_id,
                name=card[ch.name],
                surname=card[ch.surname],
                total_points=card[ch.total_points] - points_to_remove,
                remaining_points=card[ch.current_points] - points_to_remove,
                service_start_date=card[ch.service_start_date],
                service_end_date=card[ch.service_end_date],
                last_access_date=card[ch.last_access_date],
                sent_by=card[ch.sent_by],
                motivation_sent=card[ch.motivation_sent],
                other_caritas_services=card[ch.other_caritas_services],
            )
        success_popup("Famigliare eliminato!")

        # Updating table
        updated_table = self._state.anagraphic_current_viewed_family.drop(
            index=values[alk.table_family]
        ).reset_index(drop=True)
        self._state.anagraphic_current_viewed_family = updated_table

        updated_table.loc[:, fth.age] = updated_table[ah.birth_date].apply(compute_age)
        updated_table = updated_table.rename(columns=fth.map_from_anagraphic())[fth.all]
        self._window[alk.table_family].update(updated_table.values.tolist())

        # Disabling buttons if there are not more components, and clearing form
        if updated_table.shape[0] == 0:
            logging.info(
                f"All components of family id {self._state.anagraphic_current_viewed_family_id} "
                f"are deleted"
            )
            # self._anagraphic_clear_form()
            self._window[alk.button_add_component].update(disabled=True)
            self._window[alk.button_delete_component].update(disabled=True)
            self._window[alk.button_modify_component].update(disabled=True)
            self._window[alk.button_bind_card].update(disabled=True)
            self._state.anagraphic_current_viewed_family_id = None
            self._state.anagraphic_current_viewed_family = None
            self._state.anagraphic_current_insertion_family_id = None

            # Removing associated card
            if not card.empty:
                cards_controller.delete_card(card[ch.card_id])

            success_popup("Tutta la famiglia è stata eliminata.")
        return

    def _anagraphic_update_component(self, values):
        if (
            values[alk.input_name_insert] == ""
            or values[alk.input_surname_insert] == ""
            or (values[alk.radio_sex_male] and values[alk.radio_sex_female])
            or values[alk.input_birth_date_insert] == CALENDAR_DATE_DEFAULT
        ):
            warning_popup(
                "Inserire almeno Nome, Cognome, Sesso e Data di Nascita per aggiornare i dati"
            )
            return

        # Checking date format
        if not is_valid_date_format(values[alk.input_birth_date_insert].strip()):
            warning_popup(
                "La data di nascita ha un formato sbagliato.\n"
                "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
            )
            return

        # Retrieving old data to correctly identify person to update
        family_component = self._state.anagraphic_current_viewed_family.iloc[
            values[alk.table_family]
        ].squeeze()

        anagraphic_controller.update_person(
            old_name=family_component[ah.name],
            old_surname=family_component[ah.surname],
            new_name=values[alk.input_name_insert].upper().strip(),
            new_surname=values[alk.input_surname_insert].upper().strip(),
            family_id=self._state.anagraphic_current_viewed_family[
                ah.family_id
            ].unique()[0],
            new_sex="M" if values[alk.radio_sex_male] else "F",
            new_birth_date=enforce_2digits_date_format(
                values[alk.input_birth_date_insert]
            ),
            new_family_role=values[alk.input_family_role][0],
            new_residence=values[alk.input_residence],
            new_citizenship=values[alk.input_citizenship],
            new_origin_country=values[alk.input_origin_country],
            new_mobile=values[alk.input_mobile],
            new_email=values[alk.input_email],
            new_id_doc_type=values[alk.input_id_doc_type][0],
            new_id_doc_number=values[alk.input_id_doc_number],
        )

        # If the updated component is a capofamiglia, update the corresponding card (if already present)
        if family_component[ah.family_role] == "Capofamiglia":
            # Retrieve card
            card = cards_controller.search_card_by_id(
                card_id=family_component[ah.family_id]
            )
            if not card.empty:
                # Update data on card
                cards_controller.update_card(
                    card_id=family_component[ah.family_id],
                    name=values[alk.input_name_insert].strip().upper(),
                    surname=values[alk.input_surname_insert].strip().upper(),
                    total_points=card[ch.total_points],
                    remaining_points=card[ch.current_points],
                    service_start_date=enforce_2digits_date_format(
                        card[ch.service_start_date]
                    ),
                    service_end_date=enforce_2digits_date_format(
                        card[ch.service_end_date]
                    ),
                    last_access_date=card[ch.last_access_date],
                    sent_by=card[ch.sent_by],
                    motivation_sent=card[ch.motivation_sent],
                    other_caritas_services=card[ch.other_caritas_services],
                )

        # Clear fields
        self._change_anagraphic_insertion_form_state(disabled=True)

        logging.info(
            f"Updated family component {values[alk.input_name_insert]} "
            f"{values[alk.input_surname_insert]}"
        )
        success_popup("Dati aggiornati correttamente")
        return

    def _anagraphic_bind_card(self):
        """
        Bring to the Cards tab to save a new card for the family.

        Here resides the logic on the computation of the total points for the family:
        # of months of card activation * (sum of points for each adult and children)

        Points associated to each adult or children are set through environmental variables
        @return:
        """

        # Check whether the card already exists
        check_card = cards_controller.search_card_by_id(
            self._state.anagraphic_current_viewed_family_id
        )
        if not check_card.empty:
            warning_popup(
                f"Questa famiglia ha già una tessera \n"
                f"associata (numero: {check_card[ch.card_id]})."
            )
            return

        # Go to Tessere tab
        self._window[mtk.card_keys].select()
        self._window[clk.button_save_card].update(disabled=False)
        self._window[clk.button_update_card].update(disabled=True)
        self._window[clk.input_name_search].update("")
        self._window[clk.input_surname_search].update("")
        self._window[clk.input_card_id].update("")

        # Clear fields in case the were not already empty
        self._window[clk.input_end_activation_date].update(CALENDAR_DATE_DEFAULT)
        self._window[clk.input_start_activation_date].update(CALENDAR_DATE_DEFAULT)
        self._window[clk.output_last_access_date].update("Nessuna")
        self._window[clk.input_sent_by].update(
            value="Scegli un'opzione dal menù o digita nuovo inviante"
        )
        for kms in clk.cb_motivation_sent:
            self._window[kms].update(value=False)
        for kocs in clk.cb_other_caritas_services:
            self._window[kocs].update(value=False)

        # Already fill in name and surname
        capofamiglia_index = self._state.anagraphic_current_viewed_family[
            ah.family_role
        ]
        capofamiglia = self._state.anagraphic_current_viewed_family[
            capofamiglia_index == "Capofamiglia"
        ]
        self._window[clk.input_name_insert].update(capofamiglia[ah.name].values[0])
        self._window[clk.input_surname_insert].update(
            capofamiglia[ah.surname].values[0]
        )

        # Associate correct card id
        self._state.cards_current_id = self._state.anagraphic_current_viewed_family_id

        # Computing total points for selected family
        points = compute_card_points(self._state.anagraphic_current_viewed_family)

        self._window[clk.input_total_points].update(str(points))
        self._window[clk.output_remaining_points].update(str(points))
        return

    def _cards_update_data_on_card_found(self, card):
        """

        @param card:    Series with data about the card
        @return:
        """
        # Store card id
        self._state.cards_current_id = card[ch.card_id]

        # Update available actions
        self._window[clk.button_update_card].update(disabled=False)
        self._window[clk.button_print_card].update(disabled=False)
        self._window[clk.button_save_card].update(disabled=True)
        self._window[clk.button_renew_card].update(disabled=False)
        self._window[clk.text_search_response].update(
            "Tessera presente. È possibile modificarne i dati"
        )

        # Update data
        motivation_sent = card[ch.motivation_sent].split("/")
        other_caritas_services = card[ch.other_caritas_services].split("/")
        self._window[clk.input_name_insert].update(card[ch.name])
        self._window[clk.input_surname_insert].update(card[ch.surname])
        self._window[clk.input_start_activation_date].update(
            card[ch.service_start_date]
        )
        self._window[clk.input_end_activation_date].update(card[ch.service_end_date])
        self._window[clk.output_last_access_date].update(card[ch.last_access_date])
        self._window[clk.input_total_points].update(card[ch.total_points])
        self._window[clk.output_remaining_points].update(card[ch.current_points])
        self._window[clk.input_sent_by].update(value=card[ch.sent_by])
        for kms in clk.cb_motivation_sent:
            if clk.map_cb_motivation_sent_keys2values()[kms] in motivation_sent:
                self._window[kms].update(value=True)
            else:
                self._window[kms].update(value=False)
        for kocs in clk.cb_other_caritas_services:
            if (
                clk.map_cb_other_caritas_services_keys2values()[kocs]
                in other_caritas_services
            ):
                self._window[kocs].update(value=True)
            else:
                self._window[kocs].update(value=False)
        return

    def _cards_update_data_on_card_not_found(self):
        """

        @return:
        """
        self._window[clk.button_update_card].update(disabled=True)
        self._window[clk.button_print_card].update(disabled=True)
        self._window[clk.button_save_card].update(disabled=True)
        self._window[clk.button_renew_card].update(disabled=True)
        self._window[clk.text_search_response].update(
            "Tessera non presente. Associare una tessera dalla sezione Anagrafica."
        )

        # Clear fields in case the were not already empty
        self._window[clk.input_name_insert].update("")
        self._window[clk.input_surname_insert].update("")
        self._window[clk.input_start_activation_date].update(CALENDAR_DATE_DEFAULT)
        self._window[clk.input_end_activation_date].update(CALENDAR_DATE_DEFAULT)
        self._window[clk.output_last_access_date].update("Nessuna")
        self._window[clk.input_total_points].update("")
        self._window[clk.output_remaining_points].update("")
        self._window[clk.input_sent_by].update(value="")
        for kms in clk.cb_motivation_sent:
            self._window[kms].update(value=False)
        for kocs in clk.cb_other_caritas_services:
            self._window[kocs].update(value=False)
        return

    def _cards_search_by_id(self, values):
        if values[clk.input_card_id] == "":
            warning_popup("Inserire il Numero Tessera per effettuare una ricerca.")
            return

        logging.info(f"Querying for card {values[clk.input_card_id]}")
        self._window[clk.input_name_search].update("")
        self._window[clk.input_surname_search].update("")
        card = cards_controller.search_card_by_id(card_id=values[clk.input_card_id])

        if card.empty:
            logging.info("Card not present in db")
            self._cards_update_data_on_card_not_found()
        else:
            logging.info("Card found")
            self._cards_update_data_on_card_found(card)
        return

    def _cards_search_by_name(self, values):
        if (
            values[clk.input_name_search] == ""
            or values[clk.input_surname_search] == ""
        ):
            warning_popup("Inserire Nome e Cognome per effettuare una ricerca.")
            return

        logging.info(
            f"Querying for card of {values[clk.input_name_search]} {values[clk.input_surname_search]}"
        )
        self._window[clk.input_card_id].update("")
        card = cards_controller.search_card_by_name(
            name=values[clk.input_name_search], surname=values[clk.input_surname_search]
        )

        if card.empty:
            logging.info("Card not present in db")
            self._cards_update_data_on_card_not_found()
        else:
            logging.info("Card found")
            self._cards_update_data_on_card_found(card)
        return

    def _cards_save(self, values):
        # Check on inserted data
        if not is_card_data_quality_ok(values):
            return

        sent_by = "" if "'" in values[clk.input_sent_by] else values[clk.input_sent_by]

        motivation_sent = store_motivation_sent(values)
        other_caritas_services = store_other_caritas_services(values)
        cards_controller.save_card(
            card_id=self._state.cards_current_id,
            name=values[clk.input_name_insert].strip().upper(),
            surname=values[clk.input_surname_insert].strip().upper(),
            total_points=values[clk.input_total_points].strip(),
            current_points=values[clk.output_remaining_points].strip(),
            service_start_date=enforce_2digits_date_format(
                values[clk.input_start_activation_date]
            ),
            service_end_date=enforce_2digits_date_format(
                values[clk.input_end_activation_date]
            ),
            last_access_date="Nessuna",
            sent_by=sent_by,
            motivation_sent=motivation_sent,
            other_caritas_services=other_caritas_services,
        )
        logging.info(
            f"Saved card {values[clk.input_name_insert]} {values[clk.input_surname_insert]}"
        )
        success_popup("Scheda salvata correttamente!")

        # Saving current id and allowing to print the card:
        self._window[clk.button_print_card].update(disabled=False)
        self._window[clk.button_save_card].update(disabled=True)
        return

    def _cards_update_end_activation_date(self, values):
        # Check date format
        if not is_valid_date_format(values[clk.input_start_activation_date]):
            warning_popup(
                "La data di inizio servizio ha un formato sbagliato.\n"
                "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
            )
            return

        date_end = datetime.strptime(
            values[clk.input_start_activation_date], CALENDAR_DATE_FORMAT
        ) + timedelta(weeks=26)
        self._window[clk.input_end_activation_date].update(
            date_end.strftime(CALENDAR_DATE_FORMAT)
        )
        return

    def _cards_update(self, values):
        # Check on inserted data
        if not is_card_data_quality_ok(values):
            return

        motivation_sent = store_motivation_sent(values)
        other_caritas_services = store_other_caritas_services(values)
        cards_controller.update_card(
            card_id=self._state.cards_current_id,
            name=values[clk.input_name_insert].strip().upper(),
            surname=values[clk.input_surname_insert].strip().upper(),
            total_points=values[clk.input_total_points].strip(),
            remaining_points=values[clk.output_remaining_points].strip(),
            service_start_date=enforce_2digits_date_format(
                values[clk.input_start_activation_date]
            ),
            service_end_date=enforce_2digits_date_format(
                values[clk.input_end_activation_date]
            ),
            last_access_date=values[clk.output_last_access_date].strip(),
            sent_by=values[clk.input_sent_by].strip(),
            motivation_sent=motivation_sent,
            other_caritas_services=other_caritas_services,
        )

        logging.info(
            f"Updated card {self._state.cards_current_id}, {values[clk.input_name_insert]} "
            f"{values[clk.input_surname_insert]}"
        )
        success_popup("Scheda aggiornata correttamente")

        # Allowing to print the card:
        self._window[clk.button_print_card].update(disabled=False)
        return

    def _cards_renew_card(self, values):
        """
        Renew the card.

        Updates the available points for the next six months based on current family data.
        Left points are lost. New start date is today, new end date is 6 month later.
        :param values:
        :return:
        """
        # Check on inserted data
        if not is_card_data_quality_ok(values):
            return

        new_start_date = datetime.now()
        new_end_date = datetime.strftime(
            new_start_date + timedelta(weeks=26), CALENDAR_DATE_FORMAT
        )
        new_start_date = datetime.strftime(new_start_date, CALENDAR_DATE_FORMAT)

        # Retrieve family data
        family = anagraphic_controller.get_family(self._state.cards_current_id)
        family.loc[:, fth.age] = family[ah.birth_date].apply(compute_age)
        family = family.rename(columns=fth.map_from_anagraphic())[fth.all]

        # Compute points
        new_points = compute_card_points(family)

        # Update card
        motivation_sent = store_motivation_sent(values)
        other_caritas_services = store_other_caritas_services(values)
        cards_controller.update_card(
            card_id=self._state.cards_current_id,
            name=values[clk.input_name_insert].strip().upper(),
            surname=values[clk.input_surname_insert].strip().upper(),
            total_points=new_points,
            remaining_points=new_points,
            service_start_date=enforce_2digits_date_format(new_start_date),
            service_end_date=enforce_2digits_date_format(new_end_date),
            last_access_date=values[clk.output_last_access_date].strip(),
            sent_by=values[clk.input_sent_by].strip(),
            motivation_sent=motivation_sent,
            other_caritas_services=other_caritas_services,
        )

        # Update fields
        self._window[clk.input_start_activation_date].update(new_start_date)
        self._window[clk.input_end_activation_date].update(new_end_date)
        self._window[clk.input_total_points].update(new_points)
        self._window[clk.output_remaining_points].update(new_points)
        success_popup("Tessera rinnovata!")

    @staticmethod
    def _print_card(
        card_id,
        remaining_points,
        card_owner_name,
        card_owner_surname,
        service_start_date,
        service_end_date,
        last_access_date,
    ):
        """
        Util function that creates the printable card and open pdf reader.

        @param card_id:             Card id (equal to family id)
        @param remaining_points:    Points left on the card
        @param card_owner_name:     Owner name
        @param card_owner_surname:  Owner surname
        @param service_start_date:  Start date of the service
        @param service_end_date:    End date of the service
        @param last_access_date:    Last date of access to the service
        @return:                    Open pdf reader to allow printing the card
        """
        # Getting data of family (card id is same as family id)
        family = anagraphic_controller.get_family(card_id)
        family.loc[:, fth.age] = family[ah.birth_date].apply(compute_age)
        n_adults = family[family[fth.age] > 14].shape[0]
        n_children = family[family[fth.age] <= 14].shape[0]

        # Creating pdf file
        height = 125 if get_env(EnvVar.ZACCHEO_CARITAS_GROUP).lower() == "erba" else 70
        pdf = FPDF(orientation="L", unit="mm", format=(height, 80))
        pdf.set_margins(left=5, top=5, right=5)  # margins in mm
        pdf.set_auto_page_break(auto=True, margin=0.7)
        pdf.add_page()

        # Inserting logo
        if get_env(EnvVar.ZACCHEO_CARITAS_GROUP).lower() == "erba":
            pdf.image(
                f"{get_env(EnvVar.ZACCHEO_WORKING_DIR)}/resources/caritas-logo.jpg",
                y=0,
                x=20,
                w=40,
            )
            pdf.ln(h=30)

        pdf.set_font("Arial", "B", 13)
        pdf.cell(70, 10, get_env(EnvVar.ZACCHEO_CARD_CUSTOM_TITLE), align="C")
        pdf.ln(12)
        pdf.set_font("Arial", "", 10)
        pdf.cell(25, 4, "Numero tessera")
        pdf.cell(0, 4, f"{card_id}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Punti disponibili")
        pdf.cell(0, 4, f"{remaining_points}", align="R")
        pdf.ln(8)
        pdf.cell(25, 4, "Nome")
        pdf.cell(0, 4, f"{card_owner_name}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Cognome")
        pdf.cell(0, 4, f"{card_owner_surname}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Inizio servizio")
        pdf.cell(0, 4, f"{service_start_date}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Fine servizio")
        pdf.cell(0, 4, f"{service_end_date}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Data ultimo accesso")
        pdf.cell(0, 4, f"{last_access_date}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Numero adulti")
        pdf.cell(0, 4, f"{n_adults}", align="R")
        pdf.ln()
        pdf.cell(25, 4, "Numero bambini")
        pdf.cell(0, 4, f"{n_children}", align="R")
        pdf.ln(8)

        # Inserting specific information for caritas group
        if get_env(EnvVar.ZACCHEO_CARITAS_GROUP).lower() == "erba":
            pdf.set_font("Arial", "I", 9)
            pdf.multi_cell(
                0,
                9,
                "Accesso al servizio una volta ogni due mesi.\n"
                "Per appuntamento telefonare al: 3332205250.",
                align="C",
                border=1,
            )

        # Saving card and showing it
        os.makedirs(PRINTED_CARDS_FOLDER_PATH, exist_ok=True)
        output_file = os.path.join(
            PRINTED_CARDS_FOLDER_PATH,
            f"{today_to_str('%Y%m%d')}_card_id{str(card_id)}.pdf",
        )
        pdf.output(output_file, "F")

        if "win" in sys.platform:
            try:
                os.startfile(output_file)
            except IOError as ioe:
                logging.warning(ioe)
                sg.popup_ok(
                    "Per piacere, se stai già visualizzando questa tessera,\n"
                    "chiudi prima il pdf per poterla stampare di nuovo."
                )
        else:
            # For linux
            subprocess.call(["xdg-open", output_file])
        return

    def _cards_print_card(self, values):
        self._print_card(
            card_id=self._state.cards_current_id,
            remaining_points=values[clk.output_remaining_points],
            card_owner_name=values[clk.input_name_insert],
            card_owner_surname=values[clk.input_surname_insert],
            service_start_date=values[clk.input_start_activation_date],
            service_end_date=values[clk.input_end_activation_date],
            last_access_date=values[clk.output_last_access_date],
        )
        return

    def _tickets_insert_find_card(self, values):
        def clear_search_fields(window):
            window[tlk.input_insert_card_id].update("")
            window[tlk.output_last_access_date].update("")
            window[tlk.output_insert_service_end_date].update("")
            window[tlk.output_insert_available_points].update("")
            window[tlk.button_new_ticket].update(disabled=True)
            return

        if values[tlk.input_insert_card_id].strip() == "":
            warning_popup("Inserire il numero tessera per effettuare la ricerca.")
            clear_search_fields(self._window)
            return

        # Look for card id
        logging.info(f"Querying for id card {values[tlk.input_insert_card_id]}")
        card = cards_controller.search_card_by_id(
            values[tlk.input_insert_card_id].strip()
        )

        if card.empty:
            logging.info("Card not found")
            warning_popup("Tessera non presente. Effettuare una nuova ricerca")
            clear_search_fields(self._window)
        else:
            logging.info("Card found")
            self._state.tickets_loaded_card = card
            self._window[tlk.output_last_access_date].update(card[ch.last_access_date])
            self._window[tlk.output_insert_service_end_date].update(
                card[ch.service_end_date]
            )
            self._window[tlk.output_insert_available_points].update(
                card[ch.current_points]
            )
            self._window[tlk.button_new_ticket].update(disabled=False)
        return

    def _tickets_new_ticket(self):
        logging.info("Starting new ticket")
        self._window[tlk.input_target].update(disabled=False)
        self._window[tlk.input_category].update(disabled=False)
        self._window[tlk.input_product].update(values=[])
        self._window[tlk.input_product].update(disabled=False)
        self._window[tlk.input_quantity].update("")
        self._window[tlk.input_quantity].update(disabled=False)

        # Clear all other fields
        self._window[tlk.output_ticket_summary_table].update(values=[])
        self._window[tlk.button_add_article].update(disabled=True)
        self._window[tlk.button_remove_article].update(disabled=True)
        self._window[tlk.output_insert_ticket_total_points].update("0")
        self._window[tlk.button_save_and_print_ticket].update(disabled=True)
        self._state.tickets_summary_ticket = self._state.tickets_summary_ticket.drop(
            index=self._state.tickets_summary_ticket.index
        )

    def _tickets_retrieve_products(self, values):
        if len(values[tlk.input_target]) == 0 or len(values[tlk.input_category]) == 0:
            return
        else:
            products = tickets_controller.retrieve_products(
                target=values[tlk.input_target][0],
                category=values[tlk.input_category][0],
            )
            self._window[tlk.input_product].update(products)
            # Disable add article button in case there are no products
            if len(products) == 0:
                self._window[tlk.input_quantity].update("")
                self._window[tlk.button_add_article].update(disabled=True)
            return

    def _tickets_enable_add_article(self, values):
        if (
            values[tlk.input_quantity].strip() == ""
            or values[tlk.input_quantity].strip() == "0"
            or not values[tlk.input_quantity].strip().isdigit()
        ):
            self._window[tlk.button_add_article].update(disabled=True)
        else:
            self._window[tlk.button_add_article].update(disabled=False)
        return

    def _tickets_add_article(self, values):
        """
        Adds the selected product to the current ticket.

        Looks for the product in the database and retrieve its max_purchasable quantity to
        launch a warning if needed.
        """
        if (
            len(values[tlk.input_category]) == 0
            or len(values[tlk.input_target]) == 0
            or len(values[tlk.input_product]) == 0
            or values[tlk.input_quantity] == ""
        ):
            warning_popup(
                "Selezionare Destinatario, Categoria, Prodotto e \n "
                "Quantità per aggiungere l'articolo."
            )
            return

        logging.info(
            f"Adding to ticket product {values[tlk.input_product][0]}, "
            f"category {values[tlk.input_category][0]} and target {values[tlk.input_target][0]} "
            f"with quantity {values[tlk.input_quantity]}"
        )
        product = products_controller.find_product(
            product_name=values[tlk.input_product][0],
            category=values[tlk.input_category][0],
            target=values[tlk.input_target][0],
        )
        product = pd.DataFrame(
            data={product.index[i]: product[i] for i in range(0, product.shape[0])},
            index=[0],
        )

        # Raise a warning if purchased quantity is greater than max purchasable
        if int(product[ph.max_purchasable][0]) < int(values[tlk.input_quantity]):
            warning_popup("Attenzione: quantità maggiore del massimo acquistabile.")

        product.loc[:, stth.quantity] = int(values[tlk.input_quantity].strip())
        product.loc[:, stth.total_points] = int(values[tlk.input_quantity]) * int(
            product[ph.points][0]
        )
        current_article = product.rename(columns=stth.map_from_tickets()).rename(
            columns=stth.map_from_products()
        )[stth.all]
        self._state.tickets_summary_ticket = pd.concat(
            [self._state.tickets_summary_ticket, current_article]
        ).reset_index(drop=True)

        summary_ticket_to_show = copy(self._state.tickets_summary_ticket)
        summary_ticket_to_show[stth.target] = summary_ticket_to_show[stth.target].apply(
            stth.get_target_contraction
        )
        self._window[tlk.output_ticket_summary_table].update(
            values=summary_ticket_to_show.values.tolist()
        )
        self._window[tlk.output_insert_ticket_total_points].update(
            self._state.tickets_summary_ticket[stth.total_points].sum()
        )

        # Check on available card points and ticket total points with current article added
        if (
            self._state.tickets_loaded_card[ch.current_points]
            < int(values[tlk.output_insert_ticket_total_points])
            + self._state.tickets_summary_ticket[stth.total_points].tail(1).values[0]
        ):
            logging.warning("Total points of ticket larger than available points")
            self._window[tlk.output_insert_ticket_total_points].update(text_color="red")
        else:
            self._window[tlk.output_insert_ticket_total_points].update(
                text_color="black"
            )

        # Enable save ticket button and remove articles button
        self._window[tlk.button_save_and_print_ticket].update(disabled=False)
        self._window[tlk.button_remove_article].update(disabled=False)

    def _tickets_remove_article(self, values):
        if len(values[tlk.output_ticket_summary_table]) == 0:
            warning_popup("Selezionare prima un articolo dal riepilogo")
            return

        logging.info(
            f"Removing article "
            f"{self._state.tickets_summary_ticket.loc[values[tlk.output_ticket_summary_table], stth.product].values[0]}"
        )
        # Remove selected article and update ticket summary
        self._state.tickets_summary_ticket = self._state.tickets_summary_ticket.drop(
            index=values[tlk.output_ticket_summary_table]
        ).reset_index(drop=True)

        # Update summary ticket and total points
        summary_ticket_to_show = copy(self._state.tickets_summary_ticket)
        summary_ticket_to_show[stth.target] = summary_ticket_to_show[stth.target].apply(
            stth.get_target_contraction
        )
        self._window[tlk.output_ticket_summary_table].update(
            values=summary_ticket_to_show.values.tolist()
        )
        self._window[tlk.output_insert_ticket_total_points].update(
            self._state.tickets_summary_ticket[stth.total_points].sum()
        )

        # Check if remaining ticket points are more or less than available points
        if (
            self._state.tickets_summary_ticket[stth.total_points].sum()
            < self._state.tickets_loaded_card[ch.current_points]
        ):
            self._window[tlk.output_insert_ticket_total_points].update(
                text_color="black"
            )

        if self._state.tickets_summary_ticket.shape[0] == 0:
            self._window[tlk.button_remove_article].update(disabled=True)
            self._window[tlk.button_save_and_print_ticket].update(disabled=True)
        return

    def _tickets_save_and_print_ticket(self, values):
        # Warning if available card points are less than ticket total points. Allow anyway to proceed.
        if self._state.tickets_loaded_card[ch.current_points] < int(
            values[tlk.output_insert_ticket_total_points]
        ):
            warning_popup("Punti disponibili non sufficienti per l'acquisto.")

        logging.info("Saving ticket to db")
        # Saving ticket to db, one row per article
        new_ticket_id = tickets_controller.get_last_ticket_id() + 1
        for _, article in self._state.tickets_summary_ticket.iterrows():
            tickets_controller.save_ticket(
                ticket_id=new_ticket_id,
                card_id=int(values[tlk.input_insert_card_id]),
                product=article[stth.product],
                product_category=article[stth.category],
                product_target=article[stth.target],
                quantity=int(article[stth.quantity]),
                unit_points=int(article[stth.total_points] / article[stth.quantity]),
                total_points=int(article[stth.total_points]),
                date=today_to_str(CALENDAR_DATE_FORMAT),
            )

        # Update available points on card
        left_points = self._state.tickets_loaded_card[ch.current_points] - int(
            values[tlk.output_insert_ticket_total_points]
        )
        cards_controller.update_card(
            card_id=self._state.tickets_loaded_card[ch.card_id],
            name=self._state.tickets_loaded_card[ch.name],
            surname=self._state.tickets_loaded_card[ch.surname],
            total_points=self._state.tickets_loaded_card[ch.total_points],
            remaining_points=left_points,
            service_start_date=self._state.tickets_loaded_card[ch.service_start_date],
            service_end_date=self._state.tickets_loaded_card[ch.service_end_date],
            last_access_date=today_to_str(CALENDAR_DATE_FORMAT),
            sent_by=self._state.tickets_loaded_card[ch.sent_by],
            motivation_sent=self._state.tickets_loaded_card[ch.motivation_sent],
            other_caritas_services=self._state.tickets_loaded_card[
                ch.other_caritas_services
            ],
        )

        # Printing the new card
        logging.info("Printing updated ticket")
        self._print_card(
            card_id=self._state.tickets_loaded_card[ch.card_id],
            remaining_points=left_points,
            card_owner_name=self._state.tickets_loaded_card[ch.name],
            card_owner_surname=self._state.tickets_loaded_card[ch.surname],
            service_start_date=self._state.tickets_loaded_card[ch.service_start_date],
            service_end_date=self._state.tickets_loaded_card[ch.service_end_date],
            last_access_date=today_to_str(CALENDAR_DATE_FORMAT),
        )

        # Clear fields for another ticket
        self._window[tlk.input_target].update(disabled=True)
        self._window[tlk.input_category].update(disabled=True)
        self._window[tlk.input_product].update(values=[])
        self._window[tlk.input_product].update(disabled=True)
        self._window[tlk.input_quantity].update("")
        self._window[tlk.input_quantity].update(disabled=True)
        self._window[tlk.input_insert_card_id].update("")
        self._window[tlk.output_last_access_date].update("")
        self._window[tlk.output_insert_available_points].update("")
        self._window[tlk.output_insert_service_end_date].update("")
        self._window[tlk.button_new_ticket].update(disabled=True)

        # Clear all other fields
        self._window[tlk.output_ticket_summary_table].update(values=[])
        self._window[tlk.button_add_article].update(disabled=True)
        self._window[tlk.button_remove_article].update(disabled=True)
        self._window[tlk.output_insert_ticket_total_points].update("0")
        self._window[tlk.button_save_and_print_ticket].update(disabled=True)
        self._state.tickets_summary_ticket = self._state.tickets_summary_ticket.drop(
            index=self._state.tickets_summary_ticket.index
        )
        self._window[tlk.button_save_and_print_ticket].update(disabled=True)
        return

    def _tickets_view_find_card(self, values):
        def clear_fields(window):
            window[tlk.input_view_card_id].update("")
            window[tlk.output_view_service_start_date].update("")
            window[tlk.output_view_service_end_date].update("")
            window[tlk.output_view_available_points].update("")
            window[tlk.output_view_total_points_spent].update("")
            window[tlk.output_view_ticket_total_points].update("")
            window[tlk.output_past_ticket_dates_list].update(values=[])
            window[tlk.output_past_ticket_summary_table].update(values=[])
            self._state.tickets_past_tickets = None
            return

        if values[tlk.input_view_card_id].strip() == "":
            warning_popup("Inserire il numero tessera per effettuare la ricerca.")
            clear_fields(self._window)
            return

        # Look for card id
        logging.info(f"Querying for id card {values[tlk.input_view_card_id]}")
        card = cards_controller.search_card_by_id(
            values[tlk.input_view_card_id].strip()
        )

        logging.info(f"Querying for tickets of card {values[tlk.input_view_card_id]}")
        tickets = tickets_controller.get_tickets(values[tlk.input_view_card_id])

        if card.empty:
            logging.info("Card not found")
            warning_popup("Tessera non presente. Effettuare una nuova ricerca")
            clear_fields(self._window)
            # clear_search_fields(self._window)
        else:
            logging.info("Card found")

            if tickets.empty:
                logging.info("Any tickets registered")
                warning_popup("Nessuno scontrino effettuato.")
                clear_fields(self._window)
                return

            else:
                logging.info("Tickets found")
                self._state.tickets_past_tickets = tickets
                self._window[tlk.output_view_service_start_date].update(
                    card[ch.service_start_date]
                )
                self._window[tlk.output_view_service_end_date].update(
                    card[ch.service_end_date]
                )
                self._window[tlk.output_view_available_points].update(
                    card[ch.current_points]
                )
                self._window[tlk.output_view_total_points_spent].update(
                    tickets[th.total_points].sum()
                )
                self._window[tlk.output_past_ticket_dates_list].update(
                    values=tickets[th.date].unique()
                )
                self._window[tlk.output_view_ticket_total_points].update("")
                self._window[tlk.output_past_ticket_summary_table].update([])
        return

    def _tickets_show_ticket(self, values):
        logging.info(
            f"Showing ticket purchased in date {values[tlk.output_past_ticket_dates_list][0]}"
        )

        # Select ticket to show
        tickets = copy(self._state.tickets_past_tickets)
        show_ticket = tickets[
            tickets[th.date] == values[tlk.output_past_ticket_dates_list][0]
        ]
        show_ticket = show_ticket.rename(columns=th.map_to_show_ticket())[stth.all]
        show_ticket[stth.target] = show_ticket[stth.target].apply(
            stth.get_target_contraction
        )

        self._window[tlk.output_past_ticket_summary_table].update(
            values=show_ticket.values.tolist()
        )
        self._window[tlk.output_view_ticket_total_points].update(
            show_ticket[stth.total_points].sum()
        )
        return
