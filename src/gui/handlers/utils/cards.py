from datetime import datetime

import pandas as pd

from src.data.headers.anagraphic_header import FamilyTableHeader as fth
from src.data.headers.cards_header import CardsHeader as ch
from src.gui.keys.cards_keys import CardsLayoutKeys as clk
from src.gui.components.popup import warning_popup
from src.gui.utils.format import is_valid_date_format
from src.gui.layouts.constants import CALENDAR_DATE_DEFAULT, CALENDAR_DATE_FORMAT
from src.utils.time import str_to_date, compute_age
from src.config import EnvironmentalVariableNames as EnvVar, get_env


def compute_card_points(family: pd.DataFrame):
    """
    Computes the points to associate to a card for a family.

    The logic is the following:
    - computes the monthly points associated to a family, given that each adult
      has a certain amount assigned, and each child has a different amount (set
      through environmental variables)
    - the resulting monthly point is multiplied by the months of validity of the card

    Args:
        family (pd.DataFrame): family data

    Returns:
        int: points associated to the card
    """
    monthly_points = 0
    ages = family[fth.age]
    for a in ages:
        if a <= int(get_env(EnvVar.ZACCHEO_CARD_CHILD_AGE)):
            monthly_points += int(get_env(EnvVar.ZACCHEO_CARD_CHILD_POINTS))
        else:
            monthly_points += int(get_env(EnvVar.ZACCHEO_CARD_ADULT_POINTS))
    points = monthly_points * int(get_env(EnvVar.ZACCHEO_CARD_MONTHS_VALIDITY))
    return points


def points_to_update(card_data: pd.Series, birth_date: datetime):
    """
    Computes points to add to card if new component is added or to remove if component is deleted

    :param card_data:
    :param birth_date:
    :return:
    """
    service_end_date = str_to_date(card_data[ch.service_end_date], CALENDAR_DATE_FORMAT)
    now = datetime.now()
    remaining_days = (service_end_date - now).days
    remaining_months = int(round(remaining_days / 30))
    age = compute_age(birth_date)

    # Computing points
    points_per_month = (
        int(get_env(EnvVar.ZACCHEO_CARD_CHILD_POINTS))
        if age <= int(get_env(EnvVar.ZACCHEO_CARD_CHILD_AGE))
        else int(get_env(EnvVar.ZACCHEO_CARD_ADULT_POINTS))
    )
    return points_per_month * remaining_months


def check_card_points(values):
    """
    Checks whether the card remaining points are more than the card total points.

    @param values:  Values read from the window
    @return:        True if total points are larger or equal to remaining points
    """
    return (
        True
        if int(values[clk.input_total_points])
        >= int(values[clk.output_remaining_points])
        else False
    )


def is_card_data_quality_ok(values):
    # Check on default data
    if (
        values[clk.input_name_insert] == ""
        or values[clk.input_surname_insert] == ""
        or values[clk.input_start_activation_date] == CALENDAR_DATE_DEFAULT
        or values[clk.input_end_activation_date] == CALENDAR_DATE_DEFAULT
    ):
        warning_popup(
            "Inserire sia Nome, Cognome\n e date di inizio e fine attivazione servizio \n "
            "per salvare la scheda."
        )
        return False
    # Check on letters in total points
    elif not values[clk.input_total_points].strip().isdigit():
        warning_popup("Il campo 'Punti totali' deve contenere solo cifre.")
        return False

    # Check on point validity
    elif not check_card_points(values):
        warning_popup("I punti totali non posso essere maggiori dei punti rimanenti.")
        return False

    # Checking date format
    elif not is_valid_date_format(values[clk.input_start_activation_date]):
        warning_popup(
            "La data di inizio servizio ha un formato sbagliato.\n"
            "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
        )
        return False
    elif not is_valid_date_format(values[clk.input_end_activation_date]):
        warning_popup(
            "La data di fine servizio ha un formato sbagliato.\n"
            "Il formato corretto è: " + CALENDAR_DATE_DEFAULT + "."
        )
        return False
    else:
        return True


def store_motivation_sent(values):
    """
    Transform selected motivation sent into the string to store in the database.

    :param values:  All the values read from window
    :return:        String to store in the database
    """
    return "/".join(
        [
            clk.map_cb_motivation_sent_keys2values()[kms]
            for kms in clk.cb_motivation_sent
            if values[kms]
        ]
    )


def store_other_caritas_services(values):
    """
    Transform selected other caritas services into the string to store in the database.

    :param values:  All the values read from window
    :return:        String to store in the database
    """
    return "/".join(
        [
            clk.map_cb_other_caritas_services_keys2values()[kocs]
            for kocs in clk.cb_other_caritas_services
            if values[kocs]
        ]
    )
