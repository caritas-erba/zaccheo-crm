import logging

import pandas as pd

from src.data.tables import CARDS_TABLE
from src.data.db_manager import DbManager
from src.data.headers.cards_header import CardsHeader as ch
from src.gui.utils.parsing import parse_text_datum_insert, parse_text_datum_query


def search_card_by_id(card_id):
    """
    Looks for card data based on id or on name and surname.

    @param card_id:    Name
    @param surname: Surname
    @return:        Card data
    """
    query = f"SELECT * FROM {CARDS_TABLE} WHERE id_tessera = '{card_id}'"
    data = DbManager.query(query)
    if len(data) == 0:
        return pd.Series()
    else:
        data = pd.Series(data[0], index=ch.all)
    return data


def search_card_by_name(name, surname):
    """
    Looks for card data based on id or on name and surname.

    @param name:    Name
    @param surname: Surname
    @return:        Card data
    """
    query = (
        f"SELECT * FROM {CARDS_TABLE} WHERE nome = '{parse_text_datum_query(name)}' AND "
        f"cognome = '{parse_text_datum_query(surname)}'"
    )
    data = DbManager.query(query)
    if len(data) == 0:
        return pd.Series()
    else:
        data = pd.Series(data[0], index=ch.all)
    return data


def save_card(
    card_id,
    name,
    surname,
    total_points,
    current_points,
    service_start_date,
    service_end_date,
    last_access_date,
    sent_by,
    motivation_sent,
    other_caritas_services,
):
    try:
        data_tuple = (
            parse_text_datum_insert(card_id),
            parse_text_datum_insert(name),
            parse_text_datum_insert(surname),
            parse_text_datum_insert(total_points),
            parse_text_datum_insert(current_points),
            service_start_date,
            service_end_date,
            last_access_date,
            sent_by,
            motivation_sent,
            other_caritas_services,
        )
        DbManager.insert_new_row(table=CARDS_TABLE, data=data_tuple)
        return
    except Exception as e:
        logging.error(e)
        raise e


def get_last_card_id():
    """
    Look for largest card id in the cards table.

    @return:    Largest card id found, 0 if not present.
    """
    query = f"SELECT MAX({ch.card_id}) FROM {CARDS_TABLE}"
    last_id = DbManager.query(query)
    return int(last_id[0][0]) if last_id[0][0] is not None else 0


def get_card_id(name, surname):
    """
    Get card id given name and surname.
    @param name:    Name
    @param surname: Surname
    @return:        Corresponding card id
    """
    query = (
        f"SELECT {ch.card_id} FROM {CARDS_TABLE} "
        f"WHERE {ch.name} = '{parse_text_datum_query(name)}' "
        f"AND {ch.surname} = '{parse_text_datum_query(surname)}'"
    )
    card_id = DbManager.query(query)
    return int(card_id[0][0]) if card_id[0][0] is not None else None


def update_card(
    card_id,
    name,
    surname,
    total_points,
    remaining_points,
    service_start_date,
    service_end_date,
    last_access_date,
    sent_by,
    motivation_sent,
    other_caritas_services,
):
    """
    Updates a row in the cards table.
    Card_id, name and surname are used as identifier for the row.

    @param card_id:
    @param name:
    @param surname:
    @param total_points:
    @param remaining_points:
    @param service_start_date:
    @param service_end_date:
    @param last_access_date:
    @param sent_by:
    @param motivation_sent:
    @param other_caritas_services:
    @return:
    """
    old_data_id = [parse_text_datum_insert(card_id)]
    new_values = [
        parse_text_datum_insert(card_id),
        parse_text_datum_insert(name),
        parse_text_datum_insert(surname),
        parse_text_datum_insert(total_points),
        parse_text_datum_insert(remaining_points),
        service_start_date,
        service_end_date,
        last_access_date,
        sent_by,
        motivation_sent,
        other_caritas_services,
    ]
    DbManager.update_row(CARDS_TABLE, ch.all, new_values, [ch.card_id], old_data_id)
    logging.info(
        f"Updated row {', '.join([str(d) for d in old_data_id])} in table {CARDS_TABLE}"
    )
    return


def delete_card(card_id):
    DbManager.delete_row(CARDS_TABLE, [ch.card_id], [parse_text_datum_insert(card_id)])
    logging.info(f"Deleted card {card_id}")
