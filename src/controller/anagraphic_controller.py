import logging

import pandas as pd

from src.data.headers.anagraphic_header import AnagraphicHeader as ah
from src.data.tables import ANAGRAFIC_TABLE
from src.data.db_manager import DbManager
from src.gui.utils.parsing import parse_text_datum_insert, parse_text_datum_query


def get_last_family_id():
    """
    Look for largest family id in the anagraphic table.

    @return:    Largest family id found, 0 if not present.
    """
    query = f"SELECT MAX({ah.family_id}) FROM {ANAGRAFIC_TABLE}"
    last_id = DbManager.query(query)
    return int(last_id[0][0]) if last_id[0][0] is not None else 0


def save_family_component(
    family_id,
    name,
    surname,
    sex,
    birth_date="Sconosciuta",
    family_role="Capofamiglia",
    residence="Sconosciuta",
    citizenship="Sconosciuta",
    origin_country="Sconosciuta",
    mobile="Sconosciuto",
    email="Sconosciuto",
    id_doc_type="Nessuno",
    id_doc_number="Nessuno",
):
    """
    Saves a person into the anagraphic table.

    @param family_id:
    @param name:
    @param surname:
    @param sex:
    @param birth_date:
    @param family_role:
    @param residence:
    @param citizenship:
    @param origin_country:
    @param mobile:
    @param email:
    @param id_doc_type:
    @param id_doc_number:
    @return:
    """

    try:
        data_tuple = (
            parse_text_datum_insert(name),
            parse_text_datum_insert(surname),
            sex,
            birth_date,
            family_role,
            parse_text_datum_insert(residence),
            parse_text_datum_insert(citizenship),
            parse_text_datum_insert(origin_country),
            parse_text_datum_insert(mobile),
            parse_text_datum_insert(email),
            id_doc_type,
            id_doc_number,
            parse_text_datum_insert(family_id),
        )
        DbManager.insert_new_row(table=ANAGRAFIC_TABLE, data=data_tuple)
        return
    except Exception as e:
        logging.error(e)
        raise e


def get_person_family_id(name, surname, birth_date):
    """
        Retrieve family id of a person.

        @param name:
    -   @param surname:
        @param birth_date:
        @return:            Family id
    """
    query = (
        f"SELECT id_famiglia FROM {ANAGRAFIC_TABLE} "
        f"WHERE nome='{parse_text_datum_query(name)}' "
        f"AND cognome='{parse_text_datum_query(surname)}' AND "
        f"data_di_nascita='{birth_date}'"
    )
    family_id = DbManager.query(query)
    if len(family_id) == 0:
        return None
    elif len(family_id) > 1:
        raise ValueError("More than one family with the same id")
    else:
        family_id = int(family_id[0][0])
    return family_id


def get_family(family_id):
    """
    Get data of all person in a family, based on family id

    @param family_id:
    @return:            Data of all the family
    """
    query = f"SELECT * FROM {ANAGRAFIC_TABLE} WHERE id_famiglia = '{family_id}'"
    data = DbManager.query(query)
    return pd.DataFrame(data, columns=ah.all)


def update_person(
    old_name,
    new_name,
    old_surname,
    new_surname,
    family_id,
    new_sex,
    new_birth_date,
    new_family_role,
    new_residence,
    new_citizenship,
    new_origin_country,
    new_mobile,
    new_email,
    new_id_doc_type,
    new_id_doc_number,
):
    """
    Updates a row in the cards table.

    Name, surname and family_id are used as ids of the row in the table.
    """
    old_data_id = [old_name, old_surname, parse_text_datum_insert(family_id)]
    new_values = [
        parse_text_datum_insert(new_name),
        parse_text_datum_insert(new_surname),
        new_sex,
        new_birth_date,
        new_family_role,
        parse_text_datum_insert(new_residence),
        parse_text_datum_insert(new_citizenship),
        parse_text_datum_insert(new_origin_country),
        parse_text_datum_insert(new_mobile),
        parse_text_datum_insert(new_email),
        new_id_doc_type,
        new_id_doc_number,
        parse_text_datum_insert(family_id),
    ]
    DbManager.update_row(ANAGRAFIC_TABLE, ah.all, new_values, ah.ids, old_data_id)
    logging.info(
        f"Updated row {', '.join([str(d) for d in old_data_id])} in table {ANAGRAFIC_TABLE}"
    )
    return


def delete_component(name, surname, family_id):
    """

    @param family_id:
    @param name:
    @param surname:
    @return:
    """
    DbManager.delete_row(
        ANAGRAFIC_TABLE,
        ah.ids,
        [
            parse_text_datum_query(name),
            parse_text_datum_query(surname),
            parse_text_datum_insert(family_id),
        ],
    )
    logging.info(f"Deleted family component {name} {surname} of family id {family_id}")
    return
