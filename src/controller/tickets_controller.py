import logging

import pandas as pd

from src.data.tables import TICKETS_TABLE, PRODUCTS_TABLE
from src.data.db_manager import DbManager
from src.data.headers.ticket_header import TicketsHeader as th
from src.gui.utils.parsing import parse_text_datum_insert


def retrieve_products(category, target):
    """
    Looks for card data based on id or on name and surname.

    @param category:    Product category
    @param target:      Product target
    @return:            List of available products. Empty list if no products is available
    """
    query = f"SELECT prodotto FROM {PRODUCTS_TABLE} WHERE categoria = '{category}' AND destinatario = '{target}'"
    data = DbManager.query(query)
    if len(data) == 0:
        return []
    else:
        data = [data_row[0] for data_row in data]
    return data


def get_last_ticket_id():
    """
    Look for largest ticket id in the cards table.

    @return:    Largest card id found, 0 if not present.
    """
    query = f"SELECT DISTINCT MAX({th.ticket_id}) FROM {TICKETS_TABLE}"
    last_id = DbManager.query(query)
    return int(last_id[0][0]) if last_id[0][0] is not None else 0


def save_ticket(
    ticket_id,
    card_id,
    product,
    product_category,
    product_target,
    quantity,
    unit_points,
    total_points,
    date,
):
    try:
        data_tuple = (
            parse_text_datum_insert(ticket_id),
            parse_text_datum_insert(card_id),
            product,
            product_category,
            product_target,
            parse_text_datum_insert(quantity),
            parse_text_datum_insert(unit_points),
            parse_text_datum_insert(total_points),
            date,
        )
        DbManager.insert_new_row(table=TICKETS_TABLE, data=data_tuple)
        return
    except Exception as e:
        logging.error(e)
        raise e


def get_tickets(card_id):
    """
    Retrieves all the tickets of a given card.

    @param card_id: Card id
    @return:        All the tickets of that card
    """
    query = f"SELECT * FROM {TICKETS_TABLE} WHERE id_tessera = '{card_id}'"
    data = DbManager.query(query)
    if len(data) == 0:
        return pd.DataFrame()
    else:
        data = pd.DataFrame(data, columns=th.all)
    return data
