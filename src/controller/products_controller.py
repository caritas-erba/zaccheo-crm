import logging

import pandas as pd

from src.data.db_manager import DbManager
from src.data.tables import PRODUCTS_TABLE
from src.data.headers.products_header import ProductsHeader as ph
from src.gui.utils.parsing import parse_text_datum_insert, parse_text_datum_query


def create_new_product(product, category, target, description, points, max_purchasable):
    """
    Insert new product entry in the db.

    @param product:         Name of the product
    @param category:        Category
    @param target:          To who the product is addressed
    @param description:     Long description of the product (optional)
    @param points:          Cost in point of the product
    @param max_purchasable: Maximum number of purchasable products
    @return:
    """

    logging.info(f"Inserting new product {product}, {category}, {target}")
    try:
        data_tuple = (
            parse_text_datum_insert(product),
            category,
            target,
            parse_text_datum_insert(description),
            parse_text_datum_insert(points) if points != "" else "0",
            parse_text_datum_insert(max_purchasable) if max_purchasable else "1",
        )
        DbManager.insert_new_row(table=PRODUCTS_TABLE, data=data_tuple)
        return
    except Exception as e:
        logging.error(e)
        raise e


def find_products(category, target):
    """
    Looks for the currently available products in the db.

    @param category:    Category to look for available products
    @param target:      Targets to look for available products
    @return:            List of available products of the selected target and category
    """
    logging.info(f"Looking for products of category {category} and target {target}")
    query = f"SELECT prodotto FROM {PRODUCTS_TABLE} WHERE categoria = '{category}' AND destinatario = '{target}'"
    data = DbManager.query(query)
    if len(data) == 0:
        data = []
    else:
        data = [data_row[0] for data_row in data]
    return data


def find_product(product_name, category, target):
    """
    Query the db to find the specified product.

    @param product_name:    Product name
    @param category:        Category
    @param target:          Target
    @return:                If found, tuple with specified product and all its information; otherwise an empty Series
    """
    query = (
        f"SELECT * FROM {PRODUCTS_TABLE} WHERE prodotto='{parse_text_datum_query(product_name)}' "
        f"AND categoria='{category}' AND destinatario='{target}'"
    )
    data = DbManager.query(query)
    if len(data) == 0:
        return pd.Series()
    else:
        data = pd.Series(data[0], index=ph.all)
    return data


# TODO make update_* of all controllers to be a method of db manager
def update_product(
    old_product_name,
    new_product_name,
    category,
    target,
    description,
    points,
    max_purchasable,
):
    """
    Update products information.

    Arguments product, category and target are used as id to identify rows.

    @param product:
    @param category:
    @param target:
    @param description:
    @param points:
    @param max_purchasable:
    @return:
    """
    old_data_id = [old_product_name, category, target]
    new_values = [
        parse_text_datum_insert(new_product_name),
        category,
        target,
        parse_text_datum_insert(description),
        parse_text_datum_insert(points),
        parse_text_datum_insert(max_purchasable),
    ]
    DbManager.update_row(PRODUCTS_TABLE, ph.all, new_values, ph.ids, old_data_id)
    logging.info(
        f"Updated row {', '.join([str(d) for d in old_data_id])} in table {PRODUCTS_TABLE}"
    )
    return


def delete_product(product_name, target, category):
    """

    @param product_name:
    @param target:
    @param category:
    @return:
    """
    DbManager.delete_row(
        PRODUCTS_TABLE, ph.ids, [parse_text_datum_query(product_name), category, target]
    )
    logging.info(
        f"Deleted product {product_name}, with category {category} and target {target}"
    )
    return
