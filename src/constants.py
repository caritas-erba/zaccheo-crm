import os

from src.config import EnvironmentalVariableNames as EnvVar, get_env

LOG_FILE_PATH = os.path.join(get_env(EnvVar.ZACCHEO_DATA_DIR), "log", "caritas-crm.log")
DB_FILE_PATH = os.path.join(get_env(EnvVar.ZACCHEO_DATA_DIR), "db", "caritas-db.sql")
DB_DUMP_FOLDER = os.path.join(get_env(EnvVar.ZACCHEO_DATA_DIR), "db", "dump")
DB_DUMP_FILE_SUFFIX = "_caritas-db-dump.sql"

INFRASTRUCTURE_FOLDER_PATH = os.path.join(
    get_env(EnvVar.ZACCHEO_WORKING_DIR), "infrastructure"
)
PRINTED_CARDS_FOLDER_PATH = os.path.join(
    get_env(EnvVar.ZACCHEO_DATA_DIR), "printed_cards"
)

# --- Constants to write into Drive --- #
# Path of temporary directory where to save excels to be loaded to drive
EXCEL_TMP_DIR = os.path.join(get_env(EnvVar.ZACCHEO_DATA_DIR), "last_accesses")
