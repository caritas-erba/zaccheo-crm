from datetime import datetime

from src.gui.layouts.constants import CALENDAR_DATE_FORMAT


def months_difference(start_date, end_date):
    """

    @param start_date:
    @param end_date:
    @return:
    """
    start = datetime.strptime(start_date, CALENDAR_DATE_FORMAT)
    end = datetime.strptime(end_date, CALENDAR_DATE_FORMAT)
    return int((end.year - start.year) * 12 + end.month - start.month)


def compute_age(birth_date):
    """
    Computes the age given the birth date.

    @param birth_date:  Birth date of all family component.
    @return:            Ages in years
    """
    return int((datetime.now() - datetime.strptime(birth_date, CALENDAR_DATE_FORMAT)).days / 365.25)


def today_to_str(date_format: str):
    """
    Returns the today date as string with the provided format
    @param date_format:
    """
    return datetime.now().date().strftime(date_format)


def str_to_date(date: str, date_format: str):
    """
    Converts date with date_format into datetime.date
    :param date:
    :param date_format:
    :return:
    """
    return datetime.strptime(date, date_format)
