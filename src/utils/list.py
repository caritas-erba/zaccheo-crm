import math


def rearrange_list_in_columns(current_list: list, n_cols: int = 2):
    return [current_list[n : n + n_cols] for n in range(0, len(current_list), n_cols)]
