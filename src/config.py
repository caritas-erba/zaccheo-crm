import os


class EnvironmentalVariableNames:
    """Defines the names of the environmental variables used in the code and useful shortcuts"""

    # Variables to specify Caritas group. Used to customize printed cards
    ZACCHEO_CARITAS_GROUP = "ZACCHEO_CARITAS_GROUP"

    # Environmental variables for Zaccheo app
    ZACCHEO_DATA_DIR = "ZACCHEO_DATA_DIR"
    ZACCHEO_WORKING_DIR = "ZACCHEO_WORKING_DIR"

    # Variables for connection to google drive
    ZACCHEO_DRIVE_IS_ACTIVE = "ZACCHEO_DRIVE_IS_ACTIVE"
    ZACCHEO_DRIVE_TEAM_ID = "ZACCHEO_DRIVE_TEAM_ID"  # Maps TEAM_DRIVE_ID
    ZACCHEO_DRIVE_PARENT_FOLDER_ID = (
        "ZACCHEO_DRIVE_PARENT_FOLDER_ID"  # Maps DRIVE_PARENT_FOLDER_ID
    )
    # Variables to secure secrets
    ZACCHEO_DRIVE_CLIENT_ID = "ZACCHEO_DRIVE_CLIENT_ID"
    ZACCHEO_DRIVE_CLIENT_SECRET = "ZACCHEO_DRIVE_CLIENT_SECRET"

    # Varibles to generalize points calculation for cards
    ZACCHEO_CARD_ADULT_POINTS = "ZACCHEO_CARD_ADULT_POINTS"
    ZACCHEO_CARD_CHILD_POINTS = "ZACCHEO_CARD_CHILD_POINTS"
    ZACCHEO_CARD_CHILD_AGE = "ZACCHEO_CARD_CHILD_AGE"
    ZACCHEO_CARD_MONTHS_VALIDITY = "ZACCHEO_CARD_MONTHS_VALIDITY"

    # Other variables to generalize and customize cards
    ZACCHEO_CARD_CUSTOM_TITLE = "ZACCHEO_CARD_CUSTOM_TITLE"


def get_env(env_var):
    """Returns the value of the environment variable env_var"""
    return os.environ[env_var]
