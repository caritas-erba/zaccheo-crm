import sqlite3
import logging
import os

from sqlite_dump import iterdump

from src.constants import (
    DB_FILE_PATH,
    DB_DUMP_FOLDER,
    DB_DUMP_FILE_SUFFIX,
    INFRASTRUCTURE_FOLDER_PATH,
)
from src.utils.time import today_to_str


class DbManager:
    def __init__(self):
        pass

    @staticmethod
    def db_exists():
        return True if os.path.isfile(DB_FILE_PATH) else False

    @staticmethod
    def _create_empty_database():
        """Creates a new emtpy sql file"""
        conn = None
        try:
            logging.info("creating new database")
            db_folder = os.path.dirname(DB_FILE_PATH)
            os.makedirs(db_folder, exist_ok=True)
            conn = sqlite3.connect(DB_FILE_PATH)
            logging.info(f"New database created at {DB_FILE_PATH}")
        except sqlite3.Error as e:
            logging.error(e)
        finally:
            if conn:
                conn.close()

    @staticmethod
    def _connect_and_execute(query, data=None):
        conn = None
        try:
            conn = sqlite3.connect(DB_FILE_PATH)
            cur = conn.cursor()
            if data is None:
                cur.execute(query)
            else:
                cur.execute(query, data)
            conn.commit()
        except sqlite3.Error as e:
            logging.error(e)
            raise e
        finally:
            if conn:
                conn.close()

    @staticmethod
    def create_new_database():
        """Creates a new database populating it with (empty) needed tables"""
        conn = None
        try:
            logging.info("creating new database")
            db_folder = os.path.dirname(DB_FILE_PATH)
            os.makedirs(db_folder, exist_ok=True)
            conn = sqlite3.connect(DB_FILE_PATH)
            logging.info(f"New database created at {DB_FILE_PATH}")
        except sqlite3.Error as e:
            logging.error(e)
        finally:
            if conn:
                # If creation of db is ok, creates tables
                cur = conn.cursor()

                # Initializing database with empty tables
                for init_sql in os.listdir(INFRASTRUCTURE_FOLDER_PATH):
                    with open(
                        os.path.join(INFRASTRUCTURE_FOLDER_PATH, init_sql), "r"
                    ) as file:
                        init_sql_str = file.read()
                        cur.executescript(init_sql_str)

                conn.close()

    @staticmethod
    def create_dump():
        """
        Creates a dump of the db through a sql script which can be run to recreate the original db.

        @return: Write a sql script in the CARITAS_DAT_DIR/db/dump folder. Scripts are indexed by date.
        """
        today = today_to_str("%Y%m%d")
        os.makedirs(DB_DUMP_FOLDER, exist_ok=True)

        conn = sqlite3.connect(DB_FILE_PATH)
        with open(
            os.path.join(DB_DUMP_FOLDER, today + DB_DUMP_FILE_SUFFIX), "w"
        ) as sql:
            for line in iterdump(conn):
                sql.write(line + "\n")

    @staticmethod
    def insert_new_row(table: str, data: tuple):
        """
        Insert new row into specified table.

        :param table:  Table name
        :param data:   Tuple of values (with the right order) to be written
        :return:
        """

        insert_sql = (
            "INSERT INTO " + table + " VALUES (" + ("?," * len(data))[:-1] + ")"
        )
        DbManager._connect_and_execute(insert_sql, data)

    @staticmethod
    def query(sql):
        """
        Execute the input sql string.

        @param sql:     Sql string to execute
        @return:        List of tuples (corresponding to fetched rows). The list is empty if no data is found.
        """

        conn = None
        data = []
        try:
            conn = sqlite3.connect(DB_FILE_PATH)
            cur = conn.cursor()
            cur.execute(sql)
            data = cur.fetchall()
        except sqlite3.Error as e:
            logging.error(e)
            raise e
        finally:
            if conn:
                conn.close()
            return data

    @staticmethod
    def update_row(table, update_cols, update_data, condition_cols, condition_data):
        """
        Update rows in specified tables.

        @param table:           Table name
        @param update_cols:     List of columns to update
        @param update_data:     List of new values for each column
        @param condition_cols:  List of columns to identify rows to update
        @param condition_data:  List of values used to identify rows to update
        @return:
        """

        update_sql = (
            "UPDATE "
            + table
            + " SET "
            + ", ".join([col + " = ?" for col in update_cols])
            + " WHERE "
            + " AND ".join([col + " = ?" for col in condition_cols])
        )
        DbManager._connect_and_execute(update_sql, update_data + condition_data)
        return

    @staticmethod
    def delete_row(table, condition_cols, condition_values):
        """
        Deletes row in specified table.

        @param table:               Table name
        @param condition_cols:      Columns to base deletion on.
        @param condition_values:    Values of the columns to base deletion on.
        @return:
        """

        delete_sql = (
            "DELETE FROM "
            + table
            + " WHERE "
            + " AND ".join([col + " = ?" for col in condition_cols])
        )
        DbManager._connect_and_execute(delete_sql, condition_values)
        return
