import os
import logging
import json
import yaml

from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
import pandas as pd

from src.data.tables import CARDS_TABLE
from src.data.headers.cards_header import CardsHeader as ch
from src.data.db_manager import DbManager
from src.config import EnvironmentalVariableNames as EnvVar, get_env
from src.constants import EXCEL_TMP_DIR
from src.utils.time import today_to_str


def set_up_drive_credentials():
    """
    Set up google drive credentials from environmental variables to needed files.

    Stores client_id and client_secret in settings.yaml and client_secrets.json.
    """

    # Filenames
    settings_filepath = os.path.join(
        get_env(EnvVar.ZACCHEO_WORKING_DIR), "settings.yaml"
    )
    credentials_filepath = os.path.join(
        get_env(EnvVar.ZACCHEO_WORKING_DIR), "client_secrets.json"
    )

    # --- Set up credentials in settings.yaml --- #
    with open(settings_filepath, "r") as file:
        settings = yaml.safe_load(file)

    if "client_config" not in settings.keys():
        settings["client_config"] = {}
        settings["client_config"]["client_id"] = get_env(EnvVar.ZACCHEO_DRIVE_CLIENT_ID)
        settings["client_config"]["client_secret"] = get_env(
            EnvVar.ZACCHEO_DRIVE_CLIENT_SECRET
        )

    with open(settings_filepath, "w") as file:
        yaml.dump(settings, file)

    # --- Set up credentials in client_secrets.json --- #
    # Reading incomplete client secret
    with open(credentials_filepath, "r") as f:
        client_secret = json.load(f)

    # Update with credentials from system
    if "client_id" not in client_secret["web"].keys():
        client_secret["web"]["client_id"] = get_env(EnvVar.ZACCHEO_DRIVE_CLIENT_ID)
    if "client_secret" not in client_secret["web"].keys():
        client_secret["web"]["client_secret"] = get_env(
            EnvVar.ZACCHEO_DRIVE_CLIENT_SECRET
        )

    # Write to file
    with open(credentials_filepath, "w") as f:
        json.dump(client_secret, f)


def load_last_accesses_to_drive():
    """
    Loads the DataFrame with last accesses to drive as Google Sheet.

    Currently, it loads data to the specified shared drive inside the specified folder.
    Data are ordered by last access date
    """
    filename = f"{today_to_str('%Y%m%d')}_ultimi_accessi.xlsx"

    # Retrieve last accesses
    selected_columns = [
        ch.card_id,
        ch.name,
        ch.surname,
        ch.last_access_date,
        ch.service_start_date,
        ch.service_end_date,
        ch.current_points,
    ]
    query = f"SELECT {', '.join(selected_columns)} FROM {CARDS_TABLE} WHERE {ch.last_access_date} != 'Nessuna'"
    data = DbManager.query(query)
    data = pd.DataFrame(data, columns=selected_columns).sort_values(
        by=ch.last_access_date
    )

    # Authenticate to Google Drive
    logging.info("Authenticating to Google drive")
    try:
        auth = GoogleAuth()
        auth.LocalWebserverAuth()
        logging.info("Correctly authenticated")
        drive = GoogleDrive(auth)

        # Writing data to temporary excel
        os.makedirs(EXCEL_TMP_DIR, exist_ok=True)
        data.to_excel(os.path.join(EXCEL_TMP_DIR, filename), header=True, index=False)

        # Creating file to load into drive
        drive_file = drive.CreateFile(
            {
                "title": filename,
                "parents": [
                    {
                        "kind": "drive#fileLink",
                        "teamDriveId": get_env(EnvVar.ZACCHEO_DRIVE_TEAM_ID),
                        "id": get_env(EnvVar.ZACCHEO_DRIVE_PARENT_FOLDER_ID),
                    }
                ],
            }
        )

        # Loading
        drive_file.SetContentFile(os.path.join(EXCEL_TMP_DIR, filename))
        drive_file.Upload(param={"supportsTeamDrives": True, "convert": True})
        logging.info(f"Loaded file {filename} to drive")

    except Exception as e:
        logging.error(e)
