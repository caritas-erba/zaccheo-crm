
class AnagraphicHeader:

    name = "nome"
    surname = "cognome"
    sex = "sesso"
    birth_date = "data_di_nascita"
    family_role = "ruolo_famigliare"
    residence = "residenza"
    citizenship = "cittadinanza"
    origin_country = "nazione_origine"
    mobile = "cellulare"
    email = "email"
    id_document_type = "tipo_documento_identita"
    id_document_number = "numero_documento_identita"
    family_id = "id_famiglia"

    ids = [name, surname, family_id]

    all = [name, surname, sex, birth_date, family_role, residence, citizenship, origin_country,
           mobile, email, id_document_type, id_document_number, family_id]

    sex_set = ["M", "F"]
    family_role_set = ["Capofamiglia", "Moglie", "Marito", "Figlio", "Figlia"]
    id_document_type_set = ["Carta Identità", "Passaporto", "Permesso di Soggiorno", "Nessuno"]

    def __init__(self):
        pass


class FamilyTableHeader:

    name = "Nome"
    surname = "Cognome"
    age = "Età"
    sex = "Sesso"
    family_role = "Ruolo famigliare"

    all = [name, surname, age, sex, family_role]

    column_sizes = [15, 15, 5, 5, 16]

    def __init__(self):
        pass

    @staticmethod
    def map_from_anagraphic():
        return {AnagraphicHeader.name: FamilyTableHeader.name,
                AnagraphicHeader.surname: FamilyTableHeader.surname,
                AnagraphicHeader.sex: FamilyTableHeader.sex,
                AnagraphicHeader.family_role: FamilyTableHeader.family_role}
