from src.data.headers.products_header import ProductsHeader as ph


class TicketsHeader:

    ticket_id = "id_scontrino"
    card_id = "id_scheda"
    product = "prodotto"
    product_category = "categoria_prodotto"
    product_target = "destinatario_prodotto"
    quantity = "quantita"
    unit_points = "punti_unitari"
    total_points = "punti_totali"
    date = "data"

    all = [ticket_id, card_id, product, product_category, product_target, quantity, unit_points,
           total_points, date]

    @staticmethod
    def map_to_show_ticket():
        return {TicketsHeader.product: SummaryTicketTableHeader.product,
                TicketsHeader.product_category: SummaryTicketTableHeader.category,
                TicketsHeader.product_target: SummaryTicketTableHeader.target,
                TicketsHeader.quantity: SummaryTicketTableHeader.quantity,
                TicketsHeader.total_points: SummaryTicketTableHeader.total_points}

    def __init__(self):
        pass


class SummaryTicketTableHeader:

    product = "Prodotto"
    category = "Categoria"
    target = "Destinatario"
    quantity = "Qtà"
    total_points = "Pti Totali"

    all = [product, category, target, quantity, total_points]

    column_sizes = [17, 12, 12, 5, 10]

    def __init__(self):
        pass

    @staticmethod
    def map_from_tickets():
        return {TicketsHeader.product: SummaryTicketTableHeader.product,
                TicketsHeader.quantity: SummaryTicketTableHeader.quantity}

    @staticmethod
    def map_from_products():
        return {ph.category: SummaryTicketTableHeader.category,
                ph.target: SummaryTicketTableHeader.target}

    @staticmethod
    def get_target_contraction(target):
        return ph.map_target_to_target_short()[target]
