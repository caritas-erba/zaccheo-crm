import logging
from logging import handlers
import os

import PySimpleGUI as sg

from src.config import EnvironmentalVariableNames as EnvVar, get_env
from src.constants import LOG_FILE_PATH
from src.data.db_manager import DbManager
from src.gui.keys.constants import EXIT_EVENT
from src.gui.layouts.main_layout import MainLayout
from src.gui.handlers.event_handler import EventHandler
from src.gui.components.popup import error_popup
from src.data.drive_manager import set_up_drive_credentials, load_last_accesses_to_drive


# Logging configs
os.makedirs(os.path.dirname(LOG_FILE_PATH), exist_ok=True)
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s | [%(levelname)s] | %(message)s | function: %(funcName)s",
    handlers=[
        handlers.TimedRotatingFileHandler(filename=LOG_FILE_PATH, when="D"),
        logging.StreamHandler(),
    ],
)


def main():

    logging.info("Started Zaccheo CRM")

    # Setting working directory
    os.chdir(get_env(EnvVar.ZACCHEO_WORKING_DIR))

    # Create DB Manager and check for existence of db
    if not DbManager.db_exists():
        DbManager.create_new_database()
        logging.info("Created new empty db")

    # Create the window
    window = sg.Window(
        "Zaccheo CRM",
        MainLayout.layout,
        size=(850, 640),
        resizable=True,
        icon=os.path.join(
            get_env(EnvVar.ZACCHEO_WORKING_DIR), "resources", "caritas-logo.ico"
        ),
    )

    # Instantiating event handler
    event_handler = EventHandler(window)

    # Display and interact with the Window using an Event Loop
    while True:
        event, values = window.read()
        # See if user wants to quit or window was closed
        if event == sg.WINDOW_CLOSED or event == EXIT_EVENT:
            # Creating a dump of the db and exiting
            DbManager.create_dump()
            # Load files to Drive
            if get_env(EnvVar.ZACCHEO_DRIVE_IS_ACTIVE).lower() in ["true", 1, "t"]:
                set_up_drive_credentials()
                load_last_accesses_to_drive()
            break
        try:
            event_handler.handle(event, values)
        except Exception as e:
            # Output a message to the window
            logging.error(e)
            error_popup("Mi spiace, si è verificato un errore.")


if __name__ == "__main__":
    main()
