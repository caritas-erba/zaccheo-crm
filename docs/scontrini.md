# Scontrini
La pagina che permette di gestire gli scontrini si compone di due sotto-pagine: nuovo scontrino e ricerca scontrino.

## Nuovo scontrino
### Schermata
La schermata relativa alla funzione nuovo scontrino si compone di una casella di ricerca, di una parte di selezione dei prodotti per comporre lo scontrino e di una tabella riepilogativa dei prodotti inclusi nello scontrino.

Ogni scontrino si compone della lista dei prodotti acquistati, ognuno con le seguenti informazioni:
- Nome prodotto;
- Categoria del prodotto;
- Destinatario del prodotto;
- Quantità acquistata;
- Punti totali spesi per l'acquisto di quel prodotto.
Lo scontrino viene salvato associato all'id della tessera per cui è stato effettuato l'acquisto.

### Creare un nuovo scontrino
La casella di ricerca comprende il campo *Id Tessera*, in cui inserire l'id della tessera dell'utente per cui si vuole effettuare un nuovo scontrino. Una volta inserito l'id, cliccando sul pulsante *Cerca*, si visualizzano le informazioni principali associate alla tessera: *Data ultimo accesso*, *Data di fine servizio* e *Punti disponibili*. Queste informazioni sono mostrate per consentire un rapido controllo della validità della tessera e dei punti ancora disponibili per l'utente.

Una volta pronti a creare lo scontrino, premere su *Nuovo scontrino*. Vieni a questo punto attivata la parte centrale di selezione dei prodotti. Sulla sinistra sono presenti le caselle per selezionare il *Destinatario* e la *Categoria* del prodotto cercato. Quando una coppia di valori viene selezionata, la casella sulla destra viene popolata con l'elenco dei prodotti disponibili per quella scelta di destinatario e categoria.

Per aggiungere un prodotto allo scontrino, selezionare dalla lista il prodotto richiesto e inserire nella casella sottostante la quantità acquistata. Inseriti questi dati è possibile aggiungere il prodotto allo scontrino premendo *Aggiungi articolo*. Una volta premuto, comparirà nella tabella di riepilogo una voce corrispondente al prodotto appena inserito.

Se la quantità acquistata è superiore alla massima quantità acqusitabile che da catalogo è stata associata al prodotto viene mostrato un messaggio di avviso. Questo non impedisce di registrare comunque quella quantità nello scontrino. L'operatore valuterà autonomamente se consentire all'acquisto di tale quantità.

I punti totali associati allo scontrino sono mostrati in una casella in basso alla schermata. Se i punti totali superano quelli disponibili sulla tessera, questo numero si colora di rosso.

Una volta terminato l'inserimento dei prodotti nello scontrino, cliccare su *Salva scontrino e stampa tessera aggiornata* per salvare lo scontrino. Lo stesso pulsante apre automaticamente il visualizzatore pdf da cui sarà possibile stampare la nuova tessera.

### Eliminare un prodotto dallo scontrino
Se è stato fatto un errore nell'inserimento di un prodotto o della sua quantità nello scontrino, è possibile correggere l'errore cancellando il prodotto in questione e inserendo di nuovo quello corretto.

Per eliminare un prodotto dallo scontrino, nella tabella rieplogativa cliccare sulla riga di quel prodotto. Una volta selezionato si attiva il pulsante *Rimuovi articolo* situato sotto la tabella. Cliccando questo pulsante il prodotto viene eliminato dallo scontrino corrente.

## Ricerca scontrino
La sotto-pagina ricerca scontrino permette di effettuare ricerche tra gli scontrini effettuati in passato da un certo utente.
### Schermata
La schermata si compone di una casella di ricerca per la tessera di interesse, un elenco di informazioni riepilogative, una casella di ricerca per lo scontrino di interesse e di una tabella che mostra i prodotti presenti nello scontrino.

### Cercare uno scontrino
Per prima cosa è necessario inserire il numero della tessera nel campo *Id Tessera* e premere sul pulsante *Cerca*. Se la tessera è presente, verranno mostrate nei campi sottostanti le informazioni associate alla tessera: inizio e fine della validità della tessera, il totale dei punti utilizzati finora e i punti ancora disponibili.

Contemporaneamente la casella di selezione dello scontrino situata sulla destra si popola con l'elenco delle date in cui, per quella tessera, è stato effettuato uno scontrino. Selezionando una data da questo elenco viene visualizzato nella tabella sottostante il dettaglio dello scontrino selezionato, e il totale dei punti associato a quello scontrino.
