# Anagrafica
La pagina che gestisce l'anagrafica degli utenti si compone di due sotto-pagine: cerca famiglia e inserisci nuova famiglia.

## Ricerca famiglia

### Schermata
La schermata relativa alla funzione ricerca famiglia si compone di una casella di ricerca, di una tabella per la visualizzazione dei risultati e di una serie di comandi per effettuare operazioni sulla famiglia selezionata.

La casella di ricerca comprende i campi *Nome*, *Cognome* e *Data di Nascita*. Bisogna sempre specificare tutte e tre queste informazioni per poter effettuare una ricerca. È possibile inserire uno qualsiasi dei componenti della famiglia per ottenere le informazioni di tutta la famiglia.

Nella tabella di visualizzazione dei risultati saranno elencati i componeti della famiglia selezionata. Le informazioni visualizzate sono: nome, cognome, età, sesso, ruolo famigliare. Sono presenti due ulteriori campi che mostrano i contatti telefonici ed e-mail del capofamiglia.

Le operazioni che è possibile effettuare su una famiglia selezionata sono le seguenti:
- modifica di un famigliare: è possibile modificare le informazioni anagrafiche associate ad uno dei componenti della famiglia;
- aggiunta famigliare: è possibile aggiungere un nuovo componente della famiglia;
- eliminazione di un famigliare: è possibile rimuovere un componente dalla famiglia;
- associare una tessera al capofamiglia: è possibile creare la tessera da associare alla famiglia.

### Ricercare una famiglia
Per effettuare una ricerca, inserire le informazioni richieste per uno dei membri della famiglia nella casella di ricerca e premere il bottone *Cerca*. Un messaggio sotto la casella informerà sul successo o insuccesso della ricerca.

Se la ricerca non ha successo, controllare le informazioni inserite nei tre campi: queste devono coincidere esattamente con quelle inserite precedentemente nel database. Se comunque la ricerca non ha successo, significa che non è presente nel database una famiglia con quel componente.

Se la ricerca ha successo, le informazioni di tutti i componenti della famiglia sono visualizzate nella tabella sottostante.

### Operazioni sulla famiglia
Per effettuare le operazioni di modifica, aggiunta e eliminazione di un famigliare è sempre necessario selezionare nella tabella la riga del famigliare che si vuole modificare/aggiungere/eliminare prima di cliccare sul bottone della relativa operazione.

Cliccando su *Modifica famigliare*, si apre la sotto-pagina relativa all'inserimento delle informazioni anagrafiche di un componente della famiglia. Modificare le informazioni desiderate e successivamente premere su *Aggiorna*.

Cliccando su *Aggiungi famigliare*, si apre la sotto-pagina relativa all'inserimento delle informazioni anagrafiche di un componente della famiglia. Inserire le informazioni relative al nuovo famigliare e successivamente cliccare su *Fine*.

Cliccando su *Elimina famigliare*, il famigliare viene eliminato.

Cliccando su *Associa Tessera al capofamiglia*, si apre la pagina *Tessere* dove è possibile registrare una nuova tessera per la famiglia selezionata. Questa operazione è disponibile solo se la famiglia non ha ancora una tessera associata.

Una volta eseguita l'operazione, se si desidera di nuovo visualizzare le informazioni della famiglia è necessario tornare alla pagina *Anagrafica*, sotto-pagina *Ricerca famiglia*. Se si sono stati modificati i famigliare (in particolare aggiungendone o eliminandone), è necessario premere di nuovo su *Cerca* per aggiornare le informazioni nella tabella di visualizzazione.

**NB**: è buona prassi concludere ogni operazione prima di effettuarne un'altra. Questo diminusice la possibilità di errori e di inserire informazioni parziali o corrotte nel database.

## Inserisci famiglia

### Schermata
La sotto-pagina di inserimento famiglia è costituita da un modulo da compilare con le informazioni relative ad ogni componente. In cima al modulo è presente il bottone *Nuova famiglia* che abilita l'inserimento delle informazioni nel form.

Le informazioni associate ad ogni componente della famiglia sono:
- Nome (obbligatorio)
- Cognome (obbligatorio)
- Sesso
- Data di Nascita (obbligatoria)
- Ruolo famigliare (obbligatorio): serve soprattutto per designare un referente della famiglia, a cui sarà intestata la tessera. I valori possibili sono: *Capofamiglia*, *Moglie*, *Marito*, *Figlio*, *Figlia*. Il capofamiglia è la persona a cui sarà intestata la tessera, e non è possibile avere due capofamiglia contemporaneamente nella stessa famiglia.
- Residenza: città e indirizzo di residenza
- Cittadinanza
- Nazione di origine
- Numero di cellulare
- E-mail
- Tipologia documento di identià
- Numero del documento di identità

### Inserire una nuova famiglia
Per inserire una nuova famiglia, premere su *Nuova famiglia*. Verranno abilitati tutti i campi del modulo per l'inserimento. Bisogna inserire un famigliare alla volta.

Se il primo famigliare è anche l'unico, terminato l'inserimento dei dati premere direttamente su *Fine*. Altrimenti, premere su *Salva famigliare e inserisci nuovo*. In questo caso il modulo si pulirà (tranne che per alcune informazioni che verosimilmente saranno uguali) e si può procedere con l'inserimento dei dati per il nuovo famigliare. E così via per tutti i famigliari. Per procedere con il corretto salvataggio della famiglia, una volta terminato l'inserimento dei dati dell'ultimo famigliare, premere su *Fine*.
