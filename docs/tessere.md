# Tessere
La pagina *Tessere* permette di registrare nuove tessere e visualizzarne le informazioni.

## Schermata
La schermata è composta da una casella di ricerca, dal modulo che permette di inserire e visualizzare le informazioni associate alla tessera, e da una serie di pulsanti per effettuare alcune operazioni.

Le informazioni associate ad una tessera sono:
- Numero tessera: numero identificativo progressivo.
- Nome: nome dell'utente a cui è intestata la tessera.
- Cognome: cognome del'utente a cui è intestata la tessera.
- Data di attivazione del servizio: data di inizio validità della tessera.
- Data di fine servizio: data di scandenza validità della tessera.
- Data ultimo accesso: data di ultimo utilizzo della tessera (e quindi di accesso al servizio).
- Punti totali: punti totali associati alla tessera.
- Punti rimanenti: punti ancora a disposizione sulla tessera.
- Inviato da: ente o associazione che ha richiesto l'accesso al servizio per la famiglia. È possibile scegliere un ente dal menù a tendina oppure digitarne uno, se non presente nella lista.
- Ragione invio: motivo per cui è stato richiesto l'accesso al servizio. I valori possibili sono: *Difficoltà economiche*, *Famiglia numerosa*, *Disoccupazione*, *Altro*.

## Cercare una tessera
Per cercare una tessera, è possibile utilizzare la casella di ricerca inserendo o il numero della tessera, oppure il nome e il cognome dell'intestatario. Cliccare sul pulsante *Cerca* di fianco all'informazione inserita.

## Inserire una nuova tessera
L'unico modo per registrare una nuova tessera è utilizzare la funzione *Associa Tessera al capofamiglia* nella pagina *Anagrafica*, sotto-pagina *Ricerca famiglia*. Così facendo si apre la pagina *Tessere*, con alcune informazioni già precompilate. Si può controllare se le informazioni già inserite sono corrette, ed eventualmente si pu modificarle, e bisogna specificare la data di inizio validità della tessera. Una volta fatto questo, cliccare sul pulsante *Salva nuova tessera*.

Per poter registrare una nuova tessera dunque le operazioni da eseguire sono:
1. inserire nel sistema una nuova famiglia;
2. una volta concluso l'inserimento, ricercare quella famiglia;
3. cliccare sul pulsante *Associa Tessera al capofamiglia*;
4. rivedere le informazioni precompilate ed eventualmente modificarle;
5. specificare una data di inizio validità;
6. salvare la tessera cliccando su *Salva nuova tessera*.

### Assegnazione dei punti
I punti associati alla tessera sono calcolati automaticamente dal sistema sulla base della composizione della famiglia e delle variabili d'ambiente configurate in fase di installazione. Le variabili d'ambiente di interesse sono:
- `ZACCHEO_CARD_ADULT_POINTS`: numero mensile di punti da associare ad ogni adulto.
- `ZACCHEO_CARD_CHILD_POINTS`: numero mensile di punti da associare ad ogni bambino.
- `ZACCHEO_CARD_CHILD_AGE`: età fino alla quale una persona è considerata bambino.
- `ZACCHEO_CARD_MONTHS_VALIDITY`: validità della tessera (in mesi).
I punti associati alla tessera sono calcolati tramite la formula:
```
 (numero_adulti * ZACCHEO_CARD_ADULT_POINTS + numero_bambini * ZACCHEO_CARD_CHILD_POINTS) * ZACCHEO_CARD_MONTHS_VALIDITY
```

### Validità della tessera
Quando si specifica la data di inizio validità della tessera, viene precompilata anche la data di scadenza, tenendo conto del valore della variabile d'ambiente `ZACCHEO_CARD_MONTHS_VALIDITY` configurata in fase d'installazione. Se si desidera ulteriormente cambiare la data di scadenza, è possibile comunque modificarla prima di salvare la tessera.

## Modificare una tessera esistente
Per modificare le informazioni relative ad una tessera già esistente, bisogna prima effettuare la ricerca per quella tessera. Una volta trovata, modificare le informazioni desiderate, poi premere sul pulsante *Aggiorna*.

## Rinnovare una tessera
Dopo aver effettuato la ricerca della tessera, premendo il pulsante *Rinnova tessera* vengono aggiornate le date di inizio e fine validità della tessera. La data di inizio validità viene impostata al giorno corrente, quella di scadenza è prorogata  rispetto al giorno corrente tenendo conto del valore della variabile d'ambiente `ZACCHEO_CARD_MONTHS_VALIDITY`.

## Stampare una tessera
Dopo aver effettuato la ricerca della tessera, premendo il pulsante *Stampa* si aprirà il visualizzatore pdf predefinito che mostrerà la tessera con le informazioni attuali. È possibile stampare la tessera tramite le funzionalità di stampa del visualizzatore pdf.

### Personalizzazione della tessera stampata
Zaccheo permette una limitata personalizzazione della tessera stampata, in modo da poter soddisfare le esigenze di diversi utilizzatori. In particoare, è possibile configurare a piacimento l'intestazione della tessera (il titolo che compare in cima) tramite la variabile d'ammbiente `ZACCHEO_CARD_CUSTOM_TITLE`. Utilizzare come valore per questa variabile la stringa scelta, tenendo presente che si raccomanda una lunghezza inferiore ai 30 caratteri (spazi compresi). È possibile inserire stringhe più lunghe, ma potrebbero richiedere più spazio di quello disponibile sullo scontrino e risultare quindi tagliate agli estremi una volta stampate.
