# Prodotti
La pagina *Prodotti* permette di effettuare ricerche sul catalogo prodotti già presente nel database, modificare prodotti e inserirne di nuovi.

## Schermata
La schermata si compone di due parti:
- una prima parte in cui è possibile effettuare la ricerca di prodotti già presenti a catalogo, o eventualmente crearne di nuovi;
- una seconda parte in cui vengono visualizzate le informazioni dei prodotti ricercati, o in cui inserire le informazioni relative ai nuovi prodotti.

### Cos'è un prodotto
Le informazioni associate ad ogni prodotto sono:
- Destinatario: a chi è rivolto il prodotto. I possibili valori sono: *adulto* (uomo, donna, ragazzi/e maggiori dell'età configurata), *bambino* (bambini/e minori dell'età configurata), *neonato*, *casa* (per tutti i capi, come tovaglie, asciugamani, ecc. che possono essere rivolti a chiunque).
- Categoria: categoria del prodotto. I possibili valori sono: *Abiti*, *Intimo*, *Notte*, *Scarpe*, *Bagno*, *Biancheria*, *Accessori*, *Neonato*.
- Nome: il nome del prodotto.
- Descrizione: una descrizione del prodotto (opzionale).
- Punti: i punti associati al prodotto.
- Max acquistabili: numero massimo di capi acquistabili. Questo **non** è un limite stringente: se, compilando uno scontrino, un utente supera questo numero per un prodotto, Zaccheo mostra un avviso senza impedire la comilazione dello scontrino. L'idea è quella di notificare l'operatore in modo che possa valutare autonomamente se l'utente può o meno acquistare i capi.
## Ricerca prodotto
Per cercare un prodotto già presente a catalogo, è necessario selezionare un *Destinatario* e una *Categoria* tra quelle presenti. Una volta selezionate entrambe, compariranno nella lista sulla destra i relativi prodotti. Selezionando uno di questi prodotti, compariranno nella parte inferiore della schermata le informazioni relative.

### Eliminare un prodotto
Per eliminare un prodotto, dopo averlo selezionato è sufficiente premere il pulsante *Elimina*.

### Modificare un prodotto
Una volta selezionato e visualizzato un prodotto già presente a catalogo, è possibile modificare le informazioni (Nome, Descrizione, Punti, MAx acquistabili) ad esso associate. Per fare ciò, è sufficiente modificare le informazioni desiderate e successivamente premere il pulsante *Aggiorna*.

Per modificare la Categoria o il Destinatario di un prodotto, è necessario eliminare il prodotto in oggetto e crearne uno nuovo seguendo la procedura per la creazione di un nuovo prodotto.

## Nuovo prodotto
Per creare un nuovo prodotto è necessario prima di tutto selezionare la Categoria e il Destinatario per il prodotto. Una volta selezionate, si può premere il pulsante *Nuovo prodotto*. Questo abilita l'inserimento delle informazioni associate al prodotto. Una volta terminato l'inserimento delle informazioni, premere il pulsante *Salva*.
