# Introduzione
Zaccheo è un software pensato per la gestione tramite tessere a punti di un emporio del vestiario solidale Caritas.
Zaccheo è costruito utilizzando come motore grafico per l'interfaccia la libreria [PySimpleGUI](https://github.com/PySimpleGUI/PySimpleGUI).

Zaccheo permette di:
- registrare gli utenti con la loro anagrafica;
- creare le tessere associate ad ogni utente;
- creare un elenco di *prodotti* ciascuno con un punteggio associato;
- effettuare gli *scontrini* dei prodotti acquistati dagli utenti con aggiornamento automatico del punteggio sulla tessera.

## Applicazione
Zaccheo è costituito da un'interfaccia grafica che contiene quattro pagine:
- anagrafica: consente di registrare nuovi utenti, effettuare ricerche e modificare i dati anagrafici;
- tessere: consente di registrare nuove tessere, modificare quelle già presenti o rinnovarle;
- prodotti: consente di registrare nuovi prodotti o modificare quelli già presenti, ed effettuare ricerche;
- scontrini: consente di registrare nuovi scontrini con i capi acquistati e visualizzare lo storico degli scontrini di un utente.

All'avvio, l'applicazione si apre mostrando la pagina *Anagrafica*.

## Note tecniche sui dati
Zaccheo produce diversi dati nella cartella specificata nella variabile d'ambiente `ZACCHEO_DATA_DIR`, configurata prima dell'installazione.
In particolare, nella cartella vengono strutturati i seguenti dati:
- cartella `db`: qui viene salvato il database `caritas-db.sql`, di tipo `sqlite`, sottostante a Zaccheo. È presente anche una cartella `dump`, che contiene una del database salvato ogni volta che si esce da Zaccheo. Questo salvataggio ha lo scopo di effettuare un backup del database, per evitare perdite di dati in caso di problemi.
- cartella `log`: contiene i log generati da Zaccheo durante l'utilizzo, suddivisi in file giornalieri. Possono essere utili per diagnosticare la causa di eventuali problemi.
- cartella `printed_cards`: contiene una copia di tutte le tessere stampate tramite Zaccheo, a scopo di registro e backup.
- cartella `last_accesses`: è presente solo se è stata attivata la funzionalità di caricamento ultimi accessi su Google Drive. Contiene i file ultimi accessi caricati su Drive al momento dell'uscita da Zaccheo.

La descrizione degli schemi delle tabelle utilizzate nel db sqlite è definita nei files contenuti nella cartella `infrastructure`.
