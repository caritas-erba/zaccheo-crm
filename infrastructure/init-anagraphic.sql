CREATE TABLE IF NOT EXISTS anagrafica (

    nome TEXT NOT NULL,
    cognome TEXT NOT NULL,
    sesso TEXT,
    data_di_nascita TEXT,
    ruolo_famigliare TEXT,
    residenza TEXT,
    cittadinanza TEXT,
    nazione_origine TEXT,
    cellulare TEXT,
    email TEXT,
    tipo_documento_identita TEXT,
    numero_documento_identita TEXT,
    id_famiglia INTEGER NOT NULL,

    PRIMARY KEY (nome, cognome, id_famiglia)

);