CREATE TABLE IF NOT EXISTS scontrini (

    id_scontrino INTEGER NOT NULL,
    id_tessera INTEGER NOT NULL,
    prodotto TEXT NOT NULL,
    categoria_prodotto TEXT NOT NULL,
    destinatario_prodotto TEXT NOT NULL,
    quantita INTEGER NOT NULL,
    punti_unitari INTEGER NOT NULL,
    punti_totali INTEGER NOT NULL,
    data TEXT NOT NULL

);