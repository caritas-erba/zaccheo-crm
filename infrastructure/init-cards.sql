CREATE TABLE IF NOT EXISTS tessere (

    id_tessera INTEGER NOT NULL,
    nome TEXT NOT NULL,
    cognome TEXT NOT NULL,
    punti_totali_periodo INTEGER NOT NULL,
    punti_attuali INTEGER NOT NULL DEFAULT 0,
    data_inizio_attivazione TEXT NOT NULL,
    data_fine_attivazione TEXT NOT NULL,
    data_ultimo_accesso TEXT,
    inviato_da TEXT,
    motivazione_invio TEXT,
    altri_servizi_caritas TEXT,

    PRIMARY KEY (id_tessera)
);