CREATE TABLE IF NOT EXISTS prodotti (

    prodotto TEXT NOT NULL,
    categoria TEXT NOT NULL,
    destinatario TEXT NOT NULL,
    descrizione TEXT,
    punti INTEGER DEFAULT 0,
    max_acquistabili INTEGER DEFAULT 1,
    PRIMARY KEY (prodotto, categoria, destinatario)

);