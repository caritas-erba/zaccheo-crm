# Changelog

<!--next-version-placeholder-->

## v0.1.3 (2023-03-10)
### Fix
* **controller:** Passing numbers as strings to sql queries to make bind parameters work; formatted code ([`870f6d6`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/870f6d627eb09700e4d4861cba94288403cb09ad))

## v0.1.2 (2023-02-16)
### Fix
* Added missing package to installer.cfg ([`db3e80f`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/db3e80fe24f25fec6086f44b6fff132914c92087))

## v0.1.1 (2023-02-12)
### Fix
* Removed ([`6c6b78c`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/6c6b78cb662ecb8afc85dad4d62be00b2cb3a8b1))

## v0.1.0 (2023-02-12)
### Feature
* Added customization of card title ([`e433917`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/e433917e9ad187094343e1da4b76152a59e573fd))

### Fix
* Upated query to correctly escape strings ([`ff021f5`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/ff021f5bddc95a8ea2cb0f85b8de108ec751834f))
* Changed update query to correctly escape strings ([`899d740`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/899d7404c2cf5734abad195ea0220d9e3d40ee50))
* Generalized other caritas services ([`dfe9a38`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/dfe9a38e7b4e764cb9583610a9289cbe404ab760))

### Documentation
* Added explanation of new environmental variable ([`44c8e73`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/44c8e7392e6ace31e357129753eaa650c02929a2))
* **readme:** Moved section on user manual ([`576c6c0`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/576c6c09f275a38d1dda60c848e01c53dd2736c3))
* Updated readme with user manual section ([`fa87b0d`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/fa87b0dba76dbedb8a0419de3048e04dbc713896))
* Formatted ([`81af375`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/81af375171e86897fead50f73ac6776b65c2e7de))
* Added docs for scontrini page ([`861f616`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/861f616dc1d7a561ba8deba84b24feb0a08aa3a6))
* Added docs for anagrafica, tessere and prodotti ([`ead217a`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/ead217ada598064fe0c465d2a08712eb50be5b36))
* Added empty pages and completed introduzione ([`8ad72de`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/8ad72de4021ab59af4ecb18e6853e5ab439290b7))
* Updated readme ([`16ea172`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/16ea172e7a05b4006b51621770f1a9a08f899421))
* Trying to add user manual to release ([`19a33bf`](https://gitlab.com/caritas-erba/zaccheo-crm/-/commit/19a33bfc00f835a6060ada390a0d715e576004d5))
